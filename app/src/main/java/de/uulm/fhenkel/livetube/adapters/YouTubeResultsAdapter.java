package de.uulm.fhenkel.livetube.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.api.services.youtube.model.SearchResult;
import com.google.api.services.youtube.model.SearchResultSnippet;

import java.util.ArrayList;
import java.util.List;

import de.uulm.fhenkel.livetube.MyApplication;
import de.uulm.fhenkel.livetube.R;
import de.uulm.fhenkel.livetube.activities.MainActivity;

public class YouTubeResultsAdapter extends RecyclerView.Adapter<YouTubeResultsAdapter.ViewHolder> {

    private static final int TYPE_HEADER = 0;  // Declaring Variable to Understand which View is being worked on
    // IF the view under inflation and population is header or Item
    private static final int TYPE_ITEM = 1;

    private List<SearchResult> searchResults = new ArrayList<>();
    private YouTubeSearchResultsAdapter searchResultsAdapter = new YouTubeSearchResultsAdapter(searchResults);
    private ListView resultList;
    private View resultsView;
    private MainActivity activity;

    public YouTubeResultsAdapter(MainActivity activity) {
        this.activity = activity;
    }

    // Creating a ViewHolder which extends the RecyclerView View Holder
    // ViewHolder are used to to store the inflated views in order to recycle them
    public static class ViewHolder extends RecyclerView.ViewHolder {
        int Holderid;

        TextView textView;
        ImageView imageView;
        ImageView profile;


        public ViewHolder(View itemView, int ViewType) {                 // Creating ViewHolder Constructor with View and viewType As a parameter
            super(itemView);


            // Here we set the appropriate view in accordance with the the view type as passed when the holder object is created

            if (ViewType == TYPE_ITEM) {
                Holderid = 1;                                               // setting holder id as 1 as the object being populated are of type item row
            } else {
                Holderid = 0;                                                // Setting holder id = 0 as the object being populated are of type header view
            }
        }


    }


    //Below first we override the method onCreateViewHolder which is called when the ViewHolder is
    //Created, In this method we inflate the item_row.xml layout if the viewType is Type_ITEM or else we inflate header.xml
    // if the viewType is TYPE_HEADER
    // and pass it to the view holder

    @Override
    public YouTubeResultsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == TYPE_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_youtube_result, parent, false); //Inflating the layout

            ViewHolder vhItem = new ViewHolder(v, viewType); //Creating ViewHolder and passing the object of type view

            resultList = (ListView) v.findViewById(R.id.youtube_results);
            resultList.setAdapter(searchResultsAdapter);

            resultList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    SearchResult video = YouTubeResultsAdapter.this.searchResults.get(position);
                    activity.addVideo(video.getId().getVideoId());
                }
            });

            return vhItem; // Returning the created object

            //inflate your layout and pass it to view holder

        } else if (viewType == TYPE_HEADER) {

            resultsView = LayoutInflater.from(parent.getContext()).inflate(R.layout.search_header, parent, false); //Inflating the layout

            ViewHolder vhHeader = new ViewHolder(resultsView, viewType); //Creating ViewHolder and passing the object of type view

            final EditText youtubeQuery = (EditText)resultsView.findViewById(R.id.youtube_query);

            youtubeQuery.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView arg0, int arg1, KeyEvent arg2) {
                    if (arg1 == EditorInfo.IME_ACTION_SEARCH) {
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                YouTubeResultsAdapter.this.searchResults.clear();
                                YouTubeResultsAdapter.this.searchResults.addAll(MyApplication.getYouTubeDataApi().search(youtubeQuery.getText().toString()));
                                MainActivity.instance.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        searchResultsAdapter.notifyDataSetChanged();
                                    }
                                });


                            }
                        }).start();
                    }
                    return false;
                }
            });

            return vhHeader; //returning the object created


        }
        return null;

    }

    //Next we override a method which is called when the item in a row is needed to be displayed, here the int position
    // Tells us item at which position is being constructed to be displayed and the holder id of the holder object tell us
    // which view type is being created 1 for item row
    @Override
    public void onBindViewHolder(YouTubeResultsAdapter.ViewHolder holder, int position) {
        if (holder.Holderid == 1) {
            // Result
        } else {
            // Header
        }
    }

    // This method returns the number of items present in the list
    @Override
    public int getItemCount() {
        return 2; // the number of items in the list will be +1 the titles including the header view.
    }


    // Witht the following method we check what type of view is being passed
    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position))
            return TYPE_HEADER;

        return TYPE_ITEM;
    }

    private boolean isPositionHeader(int position) {
        return position == 0;
    }

}