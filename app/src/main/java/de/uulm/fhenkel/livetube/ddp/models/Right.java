package de.uulm.fhenkel.livetube.ddp.models;

import android.util.Log;

import java.util.Map;

public class Right {

    /**
     * This is a reference to the hashmap object in our "data store"
     * so we can look up fields dynamically
     */
    private Map<String, Object> fields;
    /** This is our object ID */
    private String docId;

    /**
     * Constructor for Channel object
     */
    public Right(String docId, Map<String, Object> fields) {
        this.fields = fields;
        this.docId = docId;
    }

    public String getLevel() {
        return (String)this.fields.get("level");
    }

    public String getRight() {
        return (String)this.fields.get("right");
    }

    public boolean getValue() {
        return (boolean)this.fields.get("value");
    }

}
