package de.uulm.fhenkel.livetube.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.Map;

import de.uulm.fhenkel.livetube.R;
import de.uulm.fhenkel.livetube.ddp.models.Channel;
import de.uulm.fhenkel.livetube.ddp.models.Video;

public class TopChannelListAdapter extends BaseAdapter {

    private Map<String, Channel> data;

    public TopChannelListAdapter(Map<String, Channel> map) {
        data = map;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Channel getItem(int position) {
        return (Channel)data.values().toArray()[position];
    }

    @Override
    public long getItemId(int position) {
        // TODO: Return a real id
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(parent.getContext());
            v = vi.inflate(R.layout.adapter_channel_list, parent, false);
        }

        Channel p = getItem(position);

        if (p != null) {
            TextView title = (TextView) v.findViewById(R.id.channel_name);

            if (title != null) {
                title.setText(p.getSlug());
            }
        }

        return v;
    }

}