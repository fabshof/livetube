package de.uulm.fhenkel.livetube.ddp;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;

import com.keysolutions.ddpclient.android.DDPStateSingleton;


public class MyDDPBroadcastReceiver extends BroadcastReceiver{

    private Context context;
    private DDPStateSingleton mDDP;

    public MyDDPBroadcastReceiver(DDPStateSingleton ddp, Context context) {
        this.context = context;
        this.mDDP = ddp;
        LocalBroadcastManager.getInstance(context).registerReceiver(this, new IntentFilter("ddpclient.ERROR"));
        LocalBroadcastManager.getInstance(context).registerReceiver(this, new IntentFilter("ddpclient.CONNECTIONSTATE"));
        LocalBroadcastManager.getInstance(context).registerReceiver(this, new IntentFilter("ddpclient.SUBUPDATED"));
        if(ddp.isConnected()) {
            this.onDDPConnect(ddp);
        }

    }

    public void onReceive(Context context, Intent intent) {
        Bundle bundle = intent.getExtras();
        String subscriptionName;
        if(intent.getAction().equals("ddpclient.ERROR")) {
            subscriptionName = bundle.getString("ddpclient.REASON");
            this.onError("Login Error", subscriptionName);
        } else if(intent.getAction().equals("ddpclient.CONNECTIONSTATE")) {
            int subscriptionName1 = bundle.getInt("ddpclient.STATE");
            if(subscriptionName1 == DDPStateSingleton.DDPSTATE.Closed.ordinal()) {
                this.onError("Disconnected", "Websocket to server was closed");
            } else if(subscriptionName1 == DDPStateSingleton.DDPSTATE.Connected.ordinal()) {
                this.onDDPConnect(this.mDDP);
            } else if(subscriptionName1 == DDPStateSingleton.DDPSTATE.LoggedIn.ordinal()) {
                this.onLogin();
            } else if(subscriptionName1 == DDPStateSingleton.DDPSTATE.NotLoggedIn.ordinal()) {
                this.onLogout();
            }
        } else if(intent.getAction().equals("ddpclient.SUBUPDATED")) {
            subscriptionName = bundle.getString("ddpclient.SUBNAME");
            String changeType = bundle.getString("ddpclient.CHANGETYPE");
            String docId = bundle.getString("ddpclient.CHANGEID");
            this.onSubscriptionUpdate(changeType, subscriptionName, docId);
        }

    }

    protected void onSubscriptionUpdate(String changeType, String subscriptionName, String docId) {
    }

    protected void onLogin() {
    }

    protected void onLogout() {
    }

    protected void onDDPConnect(DDPStateSingleton ddp) {
        if(!ddp.isLoggedIn()) {
            String resumeToken = ddp.getResumeToken();
            if(resumeToken != null) {
                ddp.login(resumeToken);
            }
        }

    }

    protected void onError(String title, String msg) {

    }

}
