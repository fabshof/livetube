package de.uulm.fhenkel.livetube.helpers;

import android.app.Activity;
import android.content.Context;
import android.os.CountDownTimer;
import android.os.SystemClock;
import android.util.Log;
import android.widget.TextView;

import com.google.android.youtube.player.YouTubePlayer;

import java.util.TimerTask;

import de.uulm.fhenkel.livetube.youtube.YouTubeController;

public class Timer{

    public static final String TAG = "Timer";
    public static final long COUNTDOWN_TIMER_PERIOD  = 200;

    private CountDownTimer countDownTimer;
    private YouTubeController youTubeController;
    private YouTubePlayer youTubePlayer;
    private boolean remoteMode = false;


    /**
     * The constructor
     *
     * @param youTubeController
     */
    public Timer(YouTubeController youTubeController) {
        this.youTubeController = youTubeController;
    }

    /**
     * Sets the youTubePlayer
     *
     * @param youTubePlayer
     */
    public void setYouTubePlayer(YouTubePlayer youTubePlayer) {
        this.youTubePlayer = youTubePlayer;
    }

    /**
     * Inits the timer, which calls methods of the youTubeController on even periods
     *
     * @param titleDuration
     * @param startTimeMillis
     */
    public void initTimer(final long titleDuration, final long startTimeMillis) {

        countDownTimer = new CountDownTimer(titleDuration - startTimeMillis, COUNTDOWN_TIMER_PERIOD) {
            @Override
            public void onTick(long millisUntilFinished) {

                try {

                    if (!remoteMode) {
                        // set the text of the counter and update the seekbar progress
                        youTubeController.setSeekbarText(youTubePlayer.getCurrentTimeMillis());
                        youTubeController.setSeekbarProgress(youTubePlayer.getCurrentTimeMillis());
                    } else {
                        youTubeController.setSeekbarText(titleDuration - millisUntilFinished);
                        youTubeController.setSeekbarProgress(titleDuration - millisUntilFinished);
                    }

                } catch(Exception e) {

                }
            }

            @Override
            public void onFinish() {

            }
        }.start();
    }

    /**
     * Cancels the countdowntimer and pauses the seekbar at the current position
     */
    public void pauseAtCurrentPosition(int currentPosition) {
        youTubeController.setSeekbarText(currentPosition);
        youTubeController.setSeekbarProgress(currentPosition);
    }

    /**
     * The method stops and releases the timer
     */
    public void cancelCountdownTimer() {
        if(countDownTimer != null) {
            countDownTimer.cancel();
            countDownTimer = null;
        }
    }

    public void setRemoteMode(boolean remoteMode) {
        this.remoteMode = remoteMode;
    }
}
