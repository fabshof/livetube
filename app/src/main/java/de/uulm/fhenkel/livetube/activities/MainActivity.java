package de.uulm.fhenkel.livetube.activities;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.keysolutions.ddpclient.DDPClient;
import com.keysolutions.ddpclient.DDPListener;
import com.keysolutions.ddpclient.android.DDPBroadcastReceiver;
import com.keysolutions.ddpclient.android.DDPStateSingleton;

import com.google.android.youtube.player.YouTubePlayer;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import de.uulm.fhenkel.livetube.MyApplication;
import de.uulm.fhenkel.livetube.R;
import de.uulm.fhenkel.livetube.adapters.ChannelListAdapter;
import de.uulm.fhenkel.livetube.adapters.VideoListAdapter;
import de.uulm.fhenkel.livetube.adapters.ViewPagerAdapter;
import de.uulm.fhenkel.livetube.adapters.YouTubeResultsAdapter;
import de.uulm.fhenkel.livetube.ddp.MyDDPState;
import de.uulm.fhenkel.livetube.ddp.models.Channel;
import de.uulm.fhenkel.livetube.ddp.models.Right;
import de.uulm.fhenkel.livetube.ddp.models.Video;
import de.uulm.fhenkel.livetube.fragments.ChatFragment;
import de.uulm.fhenkel.livetube.fragments.VideoListFragment;
import de.uulm.fhenkel.livetube.helpers.Helper;
import de.uulm.fhenkel.livetube.helpers.IntentHelper;
import de.uulm.fhenkel.livetube.helpers.Toaster;
import de.uulm.fhenkel.livetube.youtube.YouTubeActivity;
import de.uulm.fhenkel.livetube.youtube.YouTubeController;
import de.uulm.fhenkel.livetube.util.Config;
import de.uulm.fhenkel.livetube.youtube.YouTubeFragment;


public class MainActivity extends AppCompatActivity implements YouTubeActivity {

    public static final String TAG = "YouTubeBaseActivity";

    private String activeChannelSlug = "sopamo";

    private boolean fetchTopChannels = true;

    public static MainActivity instance;

    private YouTubeController youTubeController;
    private YouTubeFragment youTubeFragment;
    private YouTubePlayer youTubePlayer;
    private VideoListAdapter videoList;
    private ChannelListAdapter channelList;
    private DDPStateSingleton ddp;
    private TextView seekBarCountdown;
    private TextView seekBarText;
    private SeekBar seekBar;
    private DrawerLayout drawerLayout;
    private ListView drawerList;
    private ListView listview;
    DDPBroadcastReceiver broadcastReceiver;
    private VideoListFragment videoListFragment;
    private ChatFragment chatFragment;

    private Toaster toaster;

    private String currentVideo = "";
    private int currentStatus = -2;
    private Channel channel;
    private static Map<String, Video> videos = new ConcurrentHashMap<>();
    private Map<String, Video> currentVideos;
    private Map<String, Channel> topChannels;

    private DrawerLayout mDrawerLayout;
    private NavigationView mDrawerList;
    public RecyclerView mDrawerSearch;
    private ActionBarDrawerToggle mDrawerToggle;

    private CharSequence mDrawerTitle;
    private CharSequence mTitle;

    private Map<String, Right> rights;

    private Menu menu;

    private int videoIndex = 0;
    private boolean youtubePlayerIsReady = false;
    private float currentTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        instance = this;
        setContentView(R.layout.activity_main);

        activeChannelSlug = getIntent().getStringExtra("slug");

        Log.e("MainActivity", "CREATED");
        if(MyDDPState.getInstance().getCollection("videos") != null) {
            MyDDPState.getInstance().getCollection("videos").clear();
        }

        currentVideos = new ConcurrentHashMap<>();
        topChannels = new ConcurrentHashMap<>();
        rights = new ConcurrentHashMap<>();
        toaster = new Toaster(findViewById(R.id.drawer_layout));


        // Setup the navigation drawers
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        this.setupNavigationDrawer();
        this.setupSearchDrawer();

        // Check for already saved instances from the MyApplication class and restore them or instantiate them new
        youtubePlayerIsReady = false;

        if (MyApplication.getYouTubeController() == null) {

            youTubeController = new YouTubeController(this, this);

        } else {

            youTubeController = MyApplication.getYouTubeController();
            youTubeController.reinit(this, this);

        }

        if (MyApplication.getYouTubeFragment() == null) {

            youTubeFragment = YouTubeFragment.newInstance(Config.DEVELOPER_KEY, youTubeController);
            getSupportFragmentManager().beginTransaction().replace(R.id.youtube_fragment, youTubeFragment).commit();

        } else {

            youTubeFragment = MyApplication.getYouTubeFragment();
            getSupportFragmentManager().beginTransaction().replace(R.id.youtube_fragment, youTubeFragment).commit();

        }

        youtubePlayerIsReady = true;

        final FloatingActionButton playerStateImage = (FloatingActionButton) findViewById(R.id.player_state_image);
        playerStateImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (channel.getStatus() == YouTubeController.PLAYER_STATE_PLAYING) {

                    // The player is playing, pause it and call the server
                    float currentTime = youTubePlayer.getCurrentTimeMillis() / 1000f;
                    ddp.call("setVideoStatus", new Object[]{activeChannelSlug, YouTubeController.PLAYER_STATE_PAUSED, currentTime});
                    youTubeController.changePlayerStateIconToSync();

                } else {

                    // The player is paused
                    float currentTime = youTubePlayer.getCurrentTimeMillis() / 1000f;
                    ddp.call("setVideoStatus", new Object[]{activeChannelSlug, YouTubeController.PLAYER_STATE_PLAYING, currentTime});
                    youTubeController.changePlayerStateIconToSync();

                }
            }
        });

        videoList = new VideoListAdapter(currentVideos);

        // Set up the tabbing feature
        ViewPager pager = (ViewPager)findViewById(R.id.viewPager);
        setupViewPager(pager);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.modeTabs);
        tabLayout.setupWithViewPager(pager);

        // Add the icons to the single tabs
        for(int i = 0; i < tabLayout.getTabCount(); i++) {

            Drawable icon;
            // The first tab is the videolist tab
            if(i == 0) {
                icon = getDrawable(R.drawable.ic_sound);
            } else
            // The second is the chat tab
            if(i == 1) {
                icon = getDrawable(R.drawable.ic_chat);
            } else {
                icon = null;
            }

            tabLayout.getTabAt(i).setIcon(icon);
        }

        // Set the textview displaying the name and focus it to start the marquee
        seekBarText = (TextView)findViewById(R.id.seek_bar_title);
        seekBarText.setSelected(true);

        seekBar = (SeekBar) findViewById(R.id.seek_bar);
    }

    /**
     * Sets the video as the currently playing video
     *
     * @param ytVideoId
     */
    @Override
    public void playVideo(String ytVideoId) {
        if (youTubePlayer != null && MainActivity.this.ddp != null) {
            MainActivity.this.ddp.call("changeVideo", new Object[]{activeChannelSlug, ytVideoId});
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        youTubeController.stopTimer();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // If the response belongs to the login attempt
        if (requestCode == IntentHelper.LOGIN_REQUEST_CODE) {

            // A positive result came back
            if (resultCode == RESULT_OK) {

                DDPStateSingleton.DDPSTATE ddpstate = ddp.getState();

                // Check for the login state
                if (ddpstate == DDPStateSingleton.DDPSTATE.LoggedIn) {
                    toaster.showCustomText(getString(R.string.successfully_logged_in));
                } else if (ddpstate == DDPStateSingleton.DDPSTATE.NotLoggedIn){
                    toaster.showCustomText(getString(R.string.successfully_logged_out));
                } else {
                    toaster.showCustomText(getString(R.string.something_went_wrong));
                }

            }
            // Check for the user name and set it
            updateUserName();

        }
    }

    /**
     * Creates and sets up the navigation drawer
     */
    private void setupNavigationDrawer() {
        mTitle = mDrawerTitle = getTitle();
        mDrawerList = (NavigationView) findViewById(R.id.main_navigation);

        // Calculate 80 percent of the display width
        int width = (int)(getResources().getDisplayMetrics().widthPixels * 0.8f);
        DrawerLayout.LayoutParams params = (android.support.v4.widget.DrawerLayout.LayoutParams) mDrawerList.getLayoutParams();
        params.width = width;
        params.gravity = Gravity.LEFT;
        mDrawerList.setLayoutParams(params);

        // set a custom shadow that overlays the main content when the drawer opens
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);

        // enable ActionBar app icon to behave as action to toggle nav drawer
        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_drawer);
        }

        // ActionBarDrawerToggle ties together the the proper interactions
        // between the sliding drawer and the action bar app icon
        mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                mDrawerLayout,         /* DrawerLayout object */
                R.string.drawer_open,  /* "open drawer" description for accessibility */
                R.string.drawer_close  /* "close drawer" description for accessibility */
        ) {
            public void onDrawerClosed(View view) {
                getSupportActionBar().setTitle(mTitle);
                youTubeController.replayVideo();
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            public void onDrawerOpened(View drawerView) {
                getSupportActionBar().setTitle("Top Channels");
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerList.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                // Check if the user clicked on a settings item or a top channel
                if (menuItem.getGroupId() == R.id.settings) {
                    // Settings
                    switch (menuItem.getItemId()) {

                        // If the login item is pressed
                        case R.id.navigation_sub_item_0:

                            Intent loginIntent = new Intent(MainActivity.this, LoginActivity.class);
                            startActivityForResult(loginIntent, IntentHelper.LOGIN_REQUEST_CODE);
                            break;

                        case R.id.claim_channel:
                            Log.e("foo", "" + MyApplication.getUserId());
                            if (MyApplication.getUserId() != null && channel != null && channel.getOwner() == null) {
                                // Claim the channel
                                ddp.call("claim", new Object[]{channel.getSlug()}, new DDPListener() {
                                    @Override
                                    public void onResult(final Map<String, Object> resultFields) {
                                        // Check if we have results
                                        if (resultFields.get("msg").equals("result")) {
                                            (new Toaster(findViewById(android.R.id.content))).showPredefinedErrorMessage("You now own this channel.");
                                        } else {
                                            (new Toaster(findViewById(android.R.id.content))).showPredefinedErrorMessage("Could not claim this channel.");
                                        }
                                    }
                                });
                            } else if (MyApplication.getUserId() != null && channel != null && MyApplication.getUserId().equals(channel.getOwner())) {
                                // Open channel settings
                                Intent intent = new Intent(MainActivity.this, ChannelSettingsActivity.class);
                                intent.putExtra("slug", channel.getSlug());
                                startActivity(intent);
                                finish();
                            }
                            break;

                        default:
                            break;

                    }

                } else {
                    // Change the channel
                    changeChannel(menuItem.getTitle() + "");
                    mDrawerLayout.closeDrawers();
                }
                return false;
            }
        });
    }

    /**
     * Sets up the right drawer to show the youtube search
     */
    private void setupSearchDrawer() {

        mDrawerSearch = (RecyclerView) findViewById(R.id.right_drawer);
        mDrawerSearch.setHasFixedSize(true);
        mDrawerSearch.setAdapter(new YouTubeResultsAdapter(this));
        mDrawerSearch.setLayoutManager(new LinearLayoutManager(this));
        // Set the width of the sidebar
        // Calculate 80 percent of the display width
        int width = (int)(getResources().getDisplayMetrics().widthPixels * 0.8f);
        DrawerLayout.LayoutParams params = (android.support.v4.widget.DrawerLayout.LayoutParams) mDrawerSearch.getLayoutParams();
        params.width = width;
        params.gravity = Gravity.END;
        mDrawerSearch.setLayoutParams(params);
    }

    /**
     * Instantiate the ViewPager
     *
     * @param viewPager
     */
    private void setupViewPager(ViewPager viewPager) {

        // Instantiate the fragments
        String label1 = getString(R.string.tab_video_list);
        String label2 = getString(R.string.tab_chat);
        chatFragment = new ChatFragment(this, activeChannelSlug);
        videoListFragment = new VideoListFragment(videoList, this);

        // Add the fragments to the ViewPagerAdapter
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(videoListFragment, label1);
        adapter.addFrag(chatFragment, label2);
        viewPager.setAdapter(adapter);

    }

    /**
     * Adds a video to the current channel
     *
     * @param ytVideoId
     */
    public void addVideo(final String ytVideoId) {
        ddp.call("addVideo", new Object[]{activeChannelSlug, ytVideoId});
        mDrawerLayout.closeDrawers();
        Snackbar
                .make(findViewById(R.id.main_player_layout), R.string.video_added, Snackbar.LENGTH_LONG)
                .setAction(R.string.play_new_video, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        playVideo(ytVideoId);
                    }
                })
                .show();
    }

    /**
     * Setup the ddp listeners for the active channel, topChannels & channel videos
     *
     * @param channel
     */
    private void changeChannel(final String channel) {

        Log.i("MainActivity","Change channel");

        this.activeChannelSlug = channel;
        chatFragment.setActiveChannelSlug(channel);

        if(broadcastReceiver != null) {
            MyDDPState.getInstance().unsubscribe("channelVideos");
            MyDDPState.getInstance().unsubscribe("channel");
            LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver);
            broadcastReceiver = null;
        }

        // Try to get the currently active channel and set it
        if(topChannels.size() > 0) {
            Iterator it = topChannels.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry<String,Channel> entry = (Map.Entry)it.next();
                Channel c = entry.getValue();
                if(channel.equals(c.getSlug())) {
                    MainActivity.this.channel = c;
                    updateActiveChannel();
                }
            }
        }
        currentVideos.clear();
        videoListFragment.notifyDataChanged();

        if(videos.size() > 0) {
            for (Object o : videos.entrySet()) {
                Map.Entry<String, Video> entry = (Map.Entry) o;
                Video v = entry.getValue();
                if (channel.equals(v.getChannelSlug())) {
                    currentVideos.put((String) ((Map.Entry) o).getKey(), (Video) ((Map.Entry) o).getValue());
                }
            }
            videoListFragment.notifyDataChanged();
        }

        // Set the new channel title
        if(getSupportActionBar() != null) {
            mTitle = channel;
            getSupportActionBar().setTitle(mTitle);
        }

        broadcastReceiver = new DDPBroadcastReceiver(MyDDPState.getInstance(), this) {
            @Override
            protected void onDDPConnect(DDPStateSingleton ddp) {
                super.onDDPConnect(ddp);
                try {
                    MainActivity.this.ddp = ddp;
                    // add our subscriptions needed for the activity here
                    ddp.subscribe("channelVideos", new Object[]{channel});
                    ddp.subscribe("channel", new Object[]{channel});

                    // Fetch the top channels and add them to the navigation drawer
                    if (fetchTopChannels) {
                        fetchTopChannels = false;
                        ddp.call("getTopChannels", new Object[]{}, new DDPListener() {
                            @Override
                            public void onResult(final Map<String, Object> resultFields) {
                                // Check if we have results
                                if (resultFields.get("msg").equals("result")) {
                                    // Get the navigation drawer menu
                                    final Menu menu = mDrawerList.getMenu();
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            // Iterate over the top channels and add them to the menu
                                            for (Map<String, Object> topChannel : (ArrayList<Map<String, Object>>) resultFields.get("result")) {
                                                menu.add((String) topChannel.get("slug"));
                                            }
                                        }
                                    });
                                }
                            }
                        });
                    }

                    ddp.call("getRights", new Object[]{channel}, new DDPListener() {
                        @Override
                        public void onResult(final Map<String, Object> resultFields) {
                            rights.clear();
                            // Check if we have results
                            if (resultFields.get("msg").equals("result")) {
                                // Iterate over the top channels and add them to the menu
                                for (Map<String, Object> right : (ArrayList<Map<String, Object>>) resultFields.get("result")) {
                                    Right r = new Right((String) right.get("_id"), right);
                                    rights.put((String) right.get("_id"), r);
                                }
                            }
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    updateRights();
                                }
                            });
                        }
                    });
                } catch (Exception e) {
                    (new Toaster(findViewById(android.R.id.content))).showPredefinedErrorMessage("An error occured, see the error log: " + e.getMessage());
                    e.printStackTrace();
                }

            }

            @Override
            protected void onSubscriptionUpdate(String changeType, String subscriptionName, String docId) {

                try {
                    if (subscriptionName.equals("channels")) {

                        Map<String, Object> doc = ddp.getCollection(subscriptionName).get(docId);
                        if (doc.get("slug").equals(activeChannelSlug)) {
                            Log.e("foo","Updated " + activeChannelSlug + " - " + doc.get("slug") + " (" + docId + ")");
                            // Update the active channel
                            MainActivity.this.channel = new Channel(docId, ddp.getCollection(subscriptionName).get(docId));
                            updateActiveChannel();
                        }
                    } else if (subscriptionName.equals("videos")) {

                        Map<String, Object> doc = ddp.getCollection(subscriptionName).get(docId);

                        // Update the video list
                        if (changeType.equals(DDPClient.DdpMessageType.ADDED)) {
                            Video v = new Video(docId, doc);
                            v.setOrder(videoIndex++);
                            videos.put(docId, v);
                            if (v.getChannelSlug().equals(activeChannelSlug)) {
                                currentVideos.put(docId, v);
                            }
                        } else if (changeType.equals(DDPClient.DdpMessageType.REMOVED)) {
                            Video v = videos.get(docId);
                            if (v.getChannelSlug().equals(activeChannelSlug)) {
                                currentVideos.remove(docId);
                                videos.remove(docId);
                                toaster.showCustomText(getString(R.string.successfully_deleted_video));
                            }
                        } else if (changeType.equals(DDPClient.DdpMessageType.UPDATED)) {
                            //channels.get(docId).refreshFields();
                        }
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                videoListFragment.notifyDataChanged();
                            }
                        });
                    }
                } catch (Exception e) {
                    (new Toaster(findViewById(android.R.id.content))).showPredefinedErrorMessage("An error occured, see the error log: " + e.getMessage());
                    e.printStackTrace();
                }

            }

            @Override
            protected void onError(String title, String msg) {
                //super.onError(title, msg);
                Log.e("MainActivity","Error: " + msg);
            }
        };
        // we want error messages
        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver,
                new IntentFilter(MyDDPState.MESSAGE_ERROR));
        // we want connection state change messages so we know we're logged in
        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver,
                new IntentFilter(MyDDPState.MESSAGE_CONNECTION));
        MyDDPState.getInstance().connectIfNeeded();    // start connection process if we're not connected
    }

    /**
     * This method handles the events fired when something in the channel changes
     */
    private void updateActiveChannel() {
        Log.i("MainActivity","UpdateActiveChannel");
        if (!MainActivity.this.channel.getVideoId().equals(MainActivity.this.currentVideo)) {
            Log.i("MainActivity","Video changed");

            // The video changed. Load the new video.
            if (MainActivity.this.youTubePlayer != null && MainActivity.this.channel != null) {
                MainActivity.this.youTubeController.setPlayerStatus(MainActivity.this.channel.getStatus(), MainActivity.this.channel.getCurrentTime(), MainActivity.this.channel.getCurrentTimeUpdated());
                MainActivity.this.youTubeController.loadVideo(MainActivity.this.channel.getVideoId());

            }

            MainActivity.this.currentVideo = MainActivity.this.channel.getVideoId();
            MainActivity.this.currentStatus = MainActivity.this.channel.getStatus();
            MainActivity.this.currentTime = MainActivity.this.channel.getCurrentTime();

            // Retrieve the position of the active video and update the highlighted entry and the displayed text
            videoListFragment.notifyDataChanged();
            int currentPosition = getCurrentVideoPosition();
            if(currentPosition > -1 && currentPosition < videoList.getCount()) {
                Video video = videoList.getItem(currentPosition);
                seekBarText.setText(video.getTitle());
                videoList.setBackgroundActive(currentPosition);
                videoList.notifyDataSetChanged();
            }

        } else {
            // Status changed or the position changed
            if (MainActivity.this.channel.getStatus() != MainActivity.this.currentStatus || (MainActivity.this.channel.getStatus() == 1 && MainActivity.this.channel.getCurrentTime() != MainActivity.this.currentTime)) {
                Log.i("MainActivity","Playback state changed");
                // The status of the video changed. Set the new status
                MainActivity.this.youTubeController.setPlayerStatus(MainActivity.this.channel.getStatus(),MainActivity.this.channel.getCurrentTime(),MainActivity.this.channel.getCurrentTimeUpdated());
                MainActivity.this.currentStatus = MainActivity.this.channel.getStatus();
                MainActivity.this.currentTime = MainActivity.this.channel.getCurrentTime();
            }
        }
    }

    /**
     * This method activates the rights in the current channel
     */
    private void updateRights() {

        if(!hasRight("viewChannel")) {
            Intent returnIntent = new Intent();
            returnIntent.putExtra(getString(R.string.channel_return_insufficient_permissions), getString(R.string.not_allowed__channel));
            setResult(RESULT_CANCELED, returnIntent);
            finish();
        }

        if (!hasRight("changeActiveVideo")) {
            videoListFragment.disableVideoChange();
            deactivateSeekbar();
            findViewById(R.id.player_state_image).setVisibility(View.INVISIBLE);
        } else {
            videoListFragment.enableVideoChange();
            activateSeekbar();
            findViewById(R.id.player_state_image).setVisibility(View.VISIBLE);
        }

        if (!hasRight("removeVideo")) {
            videoListFragment.disableVideoDelete();
        } else {
            videoListFragment.enableVideoDelete();
        }

        if(!hasRight("addMessage")) {
            deactivateChat();
        } else {
            activateChat();
        }
    }

    /**
     * Checks if the current user has the given right
     *
     * @param level
     * @return
     */
    private boolean hasRight(String level) {
        Iterator it = rights.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();
            Right right = (Right)pair.getValue();
            if(right.getRight().equals(level)) {
                return right.getValue();
            }
        }
        return true;
    }

    /**
     * Enables the video seekbar
     */
    private void activateSeekbar() {
        seekBar.setEnabled(true);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

                youTubeController.disableProgressBarUpdate(true);
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

                // stop the timer, retrieve the current progress and set the bar to that progress
                long progressMillis = seekBar.getProgress();
                float progressF = progressMillis / 1000f;
                ddp.call("setVideoStatus", new Object[]{activeChannelSlug, YouTubeController.PLAYER_STATE_PLAYING, progressF});
                youTubeController.disableProgressBarUpdate(false);
                youTubeController.changePlayerStateIconToSync();
            }
        });
    }

    /**
     * Disables the video seekbar
     */
    private void deactivateSeekbar() {
        seekBar.setEnabled(true);
        seekBar.setOnSeekBarChangeListener(null);
    }

    /**
     * Activates the chat message field
     */
    private void activateChat() {
        findViewById(R.id.add_chat_text_wrapper).setVisibility(View.VISIBLE);
    }

    /**
     * Deactivates the chat message field
     */
    private void deactivateChat() {
        findViewById(R.id.add_chat_text_wrapper).setVisibility(View.INVISIBLE);
    }

    /**
     * The methods for an activity with a youTubeView
     */

    @Override
    public void youTubeInitFinished() {

        youtubePlayerIsReady = true;

        youTubePlayer = youTubeController.getYouTubePlayer();
        if(this.channel != null) {
            Log.e("foooo","init update active channel");
            updateActiveChannel();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);

        this.menu = menu;

        return super.onCreateOptionsMenu(menu);
    }

    /* Called whenever we call invalidateOptionsMenu() */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // If the nav drawer is open, hide action items related to the content view
        if(!hasRight("addVideo")) {
            menu.findItem(R.id.action_websearch).setVisible(false);
        } else {
            menu.findItem(R.id.action_websearch).setVisible(true);
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // The action bar home/up action should open or close the drawer.
        // ActionBarDrawerToggle will take care of this.
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        // Handle action buttons
        switch(item.getItemId()) {
            case R.id.action_websearch:
                mDrawerLayout.openDrawer(mDrawerSearch);
                return true;
            case R.id.action_remotemode:
                if(MainActivity.this.youTubeController.isRemoteMode()) {
                    item.setIcon(R.drawable.ic_action_volume_on);
                    MainActivity.this.youTubeController.setRemoteMode(false);
                } else {
                    item.setIcon(R.drawable.ic_action_volume_muted);
                    MainActivity.this.youTubeController.setRemoteMode(true);
                }

                return false;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public DDPStateSingleton getDdp() {
        return ddp;
    }

    @Override
    public String getActiveChannelSlug() {
        return activeChannelSlug;
    }

    @Override
    public int getCurrentVideoPosition() {

        int index = videoList.getCurrentVideoListPosition(currentVideo);

        return index;
    }

    @Override
    public String getNextVideoYTId() {

        int currentVideoListPosition = getCurrentVideoPosition();

        // get the next position in the videoList. If it is the last video in the list, start from the beginning
        int position = (currentVideoListPosition + 1) %  videoList.getCount();
        Video video = videoList.getItem(position);
        String ytId = video.getYTId();

        return ytId;
    }

    /**
     * The method starts a rotation animation of the playerStateImage.
     */
    @Override
    public void rotatePlayerStateImage() {

        // set the animation values
        RotateAnimation animation = new RotateAnimation(0f, 360f, Animation.RELATIVE_TO_SELF,
                0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        animation.setRepeatCount(-1);
        animation.setDuration(1000);

        // start Animation
        ImageView playerStateImage = (ImageView)findViewById(R.id.player_state_image);
        playerStateImage.startAnimation(animation);

    }

    /**
     * The function stops the rotation animation of the playerStateImage
     */
    @Override
    public void haltPlayerStateImage() {

        ImageView playerStateImage = (ImageView)findViewById(R.id.player_state_image);
        playerStateImage.setAnimation(null);

    }

    @Override
    protected void onPause() {
        super.onPause();
        if(ddp != null) {
            MyDDPState.getInstance().unsubscribe("channel");
            MyDDPState.getInstance().unsubscribe("channelVideos");
            broadcastReceiver = null;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        changeChannel(activeChannelSlug);
        updateUserName();
    }

    @Override
    protected void onStop() {
        super.onStop();
        youTubeController.stopTimer();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        if(newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            youTubePlayer.setFullscreen(true);
        } else {
            youTubePlayer.setFullscreen(false);
        }
    }

    @Override
    public boolean getIsYTPlayerReady() {
        return youtubePlayerIsReady;
    }

    @Override
    public void removeVideo(String ytId) {
        ddp.call("removeVideo", new Object[]{activeChannelSlug, ytId});
    }

    /**
     * Updates the username displayed in the sidebar
     */
    private void updateUserName() {

        TextView userNameTextView = (TextView)findViewById(R.id.user_name);
        String userName = MyApplication.getUserName();
        String textToDisplay = "Welcome";

        if (userName.equals("")) {
            textToDisplay += " !";
        } else {
            textToDisplay += ", " + userName + " !";
        }

        userNameTextView.setText(textToDisplay);
    }
}
