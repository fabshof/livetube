package de.uulm.fhenkel.livetube.youtube;

import android.app.Activity;
import android.content.Context;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.keysolutions.ddpclient.android.DDPStateSingleton;

import java.util.Calendar;

import de.uulm.fhenkel.livetube.R;
import de.uulm.fhenkel.livetube.helpers.Helper;
import de.uulm.fhenkel.livetube.helpers.Timer;
import de.uulm.fhenkel.livetube.helpers.Toaster;

public class YouTubeController implements YouTubePlayer.OnInitializedListener, YouTubePlayer.PlayerStateChangeListener, YouTubePlayer.PlaybackEventListener{

    private static final String TAG = "YouTubeController";

    public static final int PLAYER_STATE_UNSTARTED = -1;
    public static final int PLAYER_STATE_ENDED = 0;
    public static final int PLAYER_STATE_PLAYING = 1;
    public static final int PLAYER_STATE_PAUSED = 2;
    public static final int PLAYER_STATE_BUFFERING = 3;
    public static final int PLAYER_STATE_VIDEO_CUED = 4;


    private YouTubePlayer youTubePlayer;
    private YouTubeActivity youtubeActivity;
    private Activity activity;
    private int duration;
    private Context context;
    private TextView seekBarCountdownText;
    private SeekBar seekBar;
    private ImageView imageView;
    private Timer timer;
    private Toaster toaster;
    private RotateAnimation rotateAnimation;
    private Boolean progressBarIsDisabled = false;
    private boolean remoteMode = false;
    private int tempSeekbarPosition = 0;

    private int currentStatus = 0;
    private float currentTime = 0;
    private String currentVideo = null;

    private java.util.TimerTask timerTask;
    private int currentTimeUpdated;

    public YouTubeController(YouTubeActivity youtubeActivity, Context context) {

        init(youtubeActivity, context);

    }

    public YouTubePlayer getYouTubePlayer() {
        return youTubePlayer;
    }


    private void init(YouTubeActivity youtubeActivity, Context context) {

        this.youtubeActivity = youtubeActivity;
        this.context = context;

        // create the toaster, get the textview, the playerStateImage and the seekbar from the main activity
        activity = (Activity)youtubeActivity;
        toaster = new Toaster(activity.findViewById(R.id.drawer_layout));
        int idCountdown = (int)activity.getResources().getIdentifier("seek_bar_countdown", "id", context.getPackageName());
        seekBarCountdownText = (TextView)activity.findViewById(idCountdown);

        int idSeekbar = (int)activity.getResources().getIdentifier("seek_bar", "id", context.getPackageName());
        seekBar = (SeekBar)activity.findViewById(idSeekbar);

        int idPlayerStateImage = (int)activity.getResources().getIdentifier("player_state_image", "id", context.getPackageName());
        imageView = (ImageView)activity.findViewById(idPlayerStateImage);

        // create a new timer
        timer = new Timer(this);

    }

    public void reinit(YouTubeActivity youtubeActivity, Context context) {
        init(youtubeActivity, context);
    }

    /**
     * The methods for the OnInitializedListener
     */

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean wasRestored) {

        // if the player was not restored from a previously existing youTubePlayer reinitialize it
        if (!wasRestored) {
            this.youTubePlayer = youTubePlayer;
            youTubePlayer.setPlayerStyle(YouTubePlayer.PlayerStyle.CHROMELESS);
        }

        // set the listeners for the youtube player
        youTubePlayer.setPlayerStateChangeListener(this);
        youTubePlayer.setPlaybackEventListener(this);

        // call the method that determines what actions are performed when the player got initialized
        youtubeActivity.youTubeInitFinished();

        timer.setYouTubePlayer(youTubePlayer);

    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {

        Log.e(TAG, youTubeInitializationResult.toString());
        toaster.showPredefinedErrorMessage(youTubeInitializationResult.toString());

    }

    /**
     * The methods for the playerStateChangeListener
     */

    @Override
    public void onLoading() {
        Log.i("foo", "loading");
    }

    @Override
    public void onLoaded(String s) {
        Log.i("foo","loaded");

        // get the duration of the song and set the seekbar and the timer according to that
        duration = youTubePlayer.getDurationMillis();
        seekBar.setMax(duration);
        timer.initTimer(duration, getRealCurrentTime());

        if(currentStatus == PLAYER_STATE_PLAYING) {
            changePlayerStateIconToPause();
            if(!remoteMode) {
                youTubePlayer.seekToMillis(getRealCurrentTime());
                youTubePlayer.play();
            } else {
                youTubePlayer.pause();
            }
        } else if(currentStatus == PLAYER_STATE_PAUSED) {
            changePlayerStateIconToPlay();
            if(!remoteMode) {
                Log.i("foo","onLoaded: Pause");
                youTubePlayer.pause();
            } else {
                youTubePlayer.pause();
            }
        }

    }

    @Override
    public void onAdStarted() {
        Log.i("foo","ad");
    }

    @Override
    public void onVideoStarted() {
        Log.i("foo", "videostarted");

    }

    @Override
    public void onVideoEnded() {
        skipToNextVideo();
        timer.cancelCountdownTimer();
    }

    @Override
    public void onError(YouTubePlayer.ErrorReason errorReason) {
        toaster.showPredefinedErrorMessage(errorReason.toString());
    }

    /**
     * The methods for the PlayerStateChangeListener
     */

    @Override
    public void onPlaying() {
        Log.i("foo", "playing");
        if(currentStatus == PLAYER_STATE_PAUSED) {
            changePlayerStateIconToPlay();
            if(!remoteMode) {
                Log.i("foo","onPlaying: Pause");
                youTubePlayer.pause();
            }
        }
        if(remoteMode) {
            youTubePlayer.pause();
        }

    }

    @Override
    public void onPaused() {
        Log.i("foo", "paused");
    }

    @Override
    public void onStopped() {
        Log.i("foo", "stopped");
    }

    @Override
    public void onBuffering(boolean b) {
        Log.i("foo", "bufferign: " + b);

    }

    @Override
    public void onSeekTo(int i) {
        Log.i("foo","seekto: " + i);
    }

    /**
     * Own methods
     */

    /**
     * Try to start the player
     */
    public void replayVideo() {
        if(this.currentStatus == PLAYER_STATE_PLAYING) {
            youTubePlayer.play();
        }
    }

    /**
     * Skips the current video to the next in the list
     */
    public void skipToNextVideo() {

        // get the necessary parameters from the activity and notify the server to start the next video
        DDPStateSingleton ddp = youtubeActivity.getDdp();
        String activeChannelSlug = youtubeActivity.getActiveChannelSlug();
        String ytId = youtubeActivity.getNextVideoYTId();

        ddp.call("changeVideo", new Object[]{activeChannelSlug, ytId});
        ddp.call("setVideoStatus", new Object[]{activeChannelSlug, YouTubeController.PLAYER_STATE_PLAYING, 0});

    }

    /**
     * Sets the new status of the player
     *
     * @param newStatus
     */
    public void setPlayerStatus(int newStatus, float currentTime, int currentTimeUpdated) {

        // Save the current state
        this.currentStatus = newStatus;
        this.currentTime = currentTime;
        this.currentTimeUpdated = currentTimeUpdated;
        boolean playable = youtubeActivity.getIsYTPlayerReady();

        // We are in remote mode so we do not want to play the video.
        if(this.remoteMode) {
            if(newStatus == PLAYER_STATE_PLAYING) {
                timer.cancelCountdownTimer();
                timer.initTimer(youTubePlayer.getDurationMillis(), getRealCurrentTime());
                changePlayerStateIconToPause();
            } else {
                timer.cancelCountdownTimer();
                tempSeekbarPosition = seekBar.getProgress();
                timer.pauseAtCurrentPosition(tempSeekbarPosition);
                changePlayerStateIconToPlay();
            }

            youTubePlayer.pause();


            return;
        }

        switch (newStatus) {
            case PLAYER_STATE_UNSTARTED:
                break;
            case PLAYER_STATE_ENDED:
                break;
            case PLAYER_STATE_PAUSED:
                if(playable) {
                    Log.i("foo","setPlayerStatus: Pause");
                    youTubePlayer.pause();
                }
                changePlayerStateIconToPlay();
                break;
            case PLAYER_STATE_PLAYING:
                if(playable) {
                    youTubePlayer.seekToMillis(getRealCurrentTime());
                    youTubePlayer.play();
                }
                changePlayerStateIconToPause();
                break;
        }
    }

    private int getRealCurrentTime() {
        int now = (int) (System.currentTimeMillis() / 1000L);
        int time = (int)((currentTime + (now - currentTimeUpdated)) * 1000L);
        Log.e("now",now+"");
        Log.e("now2",(Calendar.getInstance().getTimeInMillis() / 1000L)+"");
        Log.e("updated",currentTimeUpdated+"");
        Log.e("diff",(now - currentTimeUpdated)+"");
        Log.e("time",time+"");
        return time;
    }

    /**
     * Loads a new video
     *
     * @param ytId
     */
    public void loadVideo(String ytId) {
        this.currentVideo = ytId;
        boolean playable = youtubeActivity.getIsYTPlayerReady();
        timer.cancelCountdownTimer();
        if(playable) {
            youTubePlayer.loadVideo(ytId);
        }
    }

    /**
     * The method disables the update of the progress bar, so that it can be dragged by the user without moving away
     *
     * @param progressBarIsDisabled
     */
    public void disableProgressBarUpdate(Boolean progressBarIsDisabled) {
        this.progressBarIsDisabled = progressBarIsDisabled;
    }

    /**
     * The method sets the countdowntext of the seekbar countdown
     *
     * @param timePassed
     */
    public void setSeekbarText(long timePassed) {
        seekBarCountdownText.setText(Helper.getSuperAwesomeTimeDisplay(timePassed));
    }

    /**
     * The method takes the progress as input and sets the bar to that progress
     *
     * @param progress
     */
    public void setSeekbarProgress(long progress) {

        // check if the user drags the seekBar. if not, change the progress
        if(!progressBarIsDisabled) {

            // get the maximum of the seekbar (=duration) and check if the progress is beyond
            int maximum = youTubePlayer.getDurationMillis();

            if(progress > maximum) {
                seekBar.setProgress(maximum);
            } else {
                seekBar.setProgress((int)progress);
            }
        }
    }

    /**
     * The method stops the timer
     */
    public void stopTimer() {
        if (timer != null) {
            timer.cancelCountdownTimer();
        }
    }

    /**
     * The method initiates the timer which starts counting from the point of time given as input
     *
     * @param startTimeMillis
     */
    public void initAndStartTimer(long startTimeMillis) {
        timer.initTimer(duration, startTimeMillis);
    }

    /**
     * The method changes the resource of the playerStateIcon
     *
     * @param name
     */
    public void changePlayerStateIcon(String name) {

        int idLoading = (int)activity.getResources().getIdentifier(name, "drawable", context.getPackageName());
        imageView.setImageResource(idLoading);

    }

    /**
     * Sets the playerStateIcon to the play icon
     */
    public void changePlayerStateIconToPlay() {

        changePlayerStateIcon("ic_action_play");
        youtubeActivity.haltPlayerStateImage();

    }

    /**
     * Sets the playerStateIcon to the pause icon
     */
    public void changePlayerStateIconToPause() {

        changePlayerStateIcon("ic_action_pause");
        youtubeActivity.haltPlayerStateImage();

    }

    /**
     * Sets the playerStateIcon to the reload icon and disabled the view so that it cannot be clicked
     */
    public void changePlayerStateIconToLoading() {

        changePlayerStateIcon("ic_action_refresh");
        youtubeActivity.rotatePlayerStateImage();

    }

    /**
     * Sets the playerStateIcon to the sync icon
     */
    public void changePlayerStateIconToSync() {

        changePlayerStateIcon("ic_action_refresh");
        youtubeActivity.rotatePlayerStateImage();

    }

    /**
     * Sets the playerStateIcon to the error icon
     */
    public void changePlayerStateIconToError() {

        changePlayerStateIcon("ic_action_warning");
        youtubeActivity.haltPlayerStateImage();

    }

    public void setRemoteMode(boolean remoteMode) {
        this.remoteMode = remoteMode;
        if(remoteMode) {
            Log.i("foo","remoteMode: Pause");
            youTubePlayer.pause();
            timer.setRemoteMode(true);
        } else {
            loadVideo(this.currentVideo);
            setPlayerStatus(this.currentStatus,this.currentTime,this.currentTimeUpdated);
        }
    }

    public boolean isRemoteMode() {
        return this.remoteMode;
    }

}
