package de.uulm.fhenkel.livetube.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.LinkedHashMap;

import de.uulm.fhenkel.livetube.R;

public class ChatListAdapter extends BaseAdapter {

    private LinkedHashMap<String, String> messages;
    private LinkedHashMap<String, String> names;

    public ChatListAdapter(LinkedHashMap<String, String> messages, LinkedHashMap<String, String> names) {
        this.messages = messages;
        this.names = names;
    }

    @Override
    public int getCount() {
        return messages.size();
    }

    @Override
    public Object getItem(int position) {
        return messages.get(String.valueOf(position));
    }

    public Object getItem(int position, LinkedHashMap<String, String> hashmap) {
        return hashmap.get(String.valueOf(position));
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(parent.getContext());
            v = vi.inflate(R.layout.adapter_chat_list, parent, false);
        }

        String message = (String)getItem(position);
        String name = (String)getItem(position, names);

        if (message != null) {
            TextView content = (TextView) v.findViewById(R.id.message_content);
            TextView owner = (TextView) v.findViewById(R.id.message_owner);

            if (content != null) {
                content.setText(message);
            }
            if (owner != null) {
                if(name != null) {
                    owner.setText(name);
                } else {

                    String unknownUser = v.getResources().getString(R.string.unknown_user);
                    owner.setText(unknownUser);

                }
            }
        }

        return v;

    }

    public LinkedHashMap<String, String> getItems() {
        return messages;
    }
}
