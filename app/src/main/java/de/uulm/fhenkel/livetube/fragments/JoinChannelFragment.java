package de.uulm.fhenkel.livetube.fragments;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.keysolutions.ddpclient.android.DDPStateSingleton;

import de.uulm.fhenkel.livetube.R;
import de.uulm.fhenkel.livetube.activities.ChannelSettingsActivity;
import de.uulm.fhenkel.livetube.activities.MainActivity;
import de.uulm.fhenkel.livetube.helpers.Toaster;

public class JoinChannelFragment extends DialogFragment {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        final View view = LayoutInflater.from(this.getActivity()).inflate(R.layout.dialog_join_channel, null);
        // Set the view for the dialog
        builder.setView(view);

        // Add and handle ok and cancel buttons
        builder
                .setPositiveButton(R.string.join_this_channel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        String slug = ((TextView) view.findViewById(R.id.channel_name)).getText().toString();
                        Intent intent = new Intent(getActivity(), MainActivity.class);
                        intent.putExtra("slug",slug);
                        startActivity(intent);
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });

        // Create the AlertDialog object and return it
        return builder.create();
    }
}
