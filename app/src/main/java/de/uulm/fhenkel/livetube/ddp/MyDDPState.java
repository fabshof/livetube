package de.uulm.fhenkel.livetube.ddp;

import android.content.Context;
import android.util.Log;

import com.keysolutions.ddpclient.DDPClient;
import com.keysolutions.ddpclient.android.DDPStateSingleton;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import de.uulm.fhenkel.livetube.ddp.models.Channel;
import de.uulm.fhenkel.livetube.ddp.models.Video;
import de.uulm.fhenkel.livetube.helpers.Hooks;

public class MyDDPState extends DDPStateSingleton {

    /**
     * Constructor for this singleton (private because it's a singleton)
     * @param context Android application context
     */
    private MyDDPState(Context context) {
        // Constructor hidden because this is a singleton
        super(context, "liveplaylistv2.meteor.com",80);
    }

    /**
     * Used by MyApplication singleton to initialize this singleton
     * @param context Android application context
     */
    public static void initInstance(Context context) {
        // only called by MyApplication
        if (mInstance == null) {
            // Create the instance
            mInstance = new MyDDPState(context);
        }
    }

    @Override
    public void broadcastSubscriptionChanged(String collectionName, String changetype, String docId) {
        super.broadcastSubscriptionChanged(collectionName, changetype, docId);
    }

    public void unsubscribe(String name) {
        this.getDDP().unsubscribe(name);
    }
    public void disconnect() {
        this.getDDP().disconnect();
    }

    public void clearObservers() {
        this.getDDP().deleteObservers();
    }

    public void setDDPState(DDPSTATE ddpState) {
        this.mDDPState = ddpState;
    }

    public static MyDDPState getInstance() {
        return (MyDDPState)mInstance;
    }
}