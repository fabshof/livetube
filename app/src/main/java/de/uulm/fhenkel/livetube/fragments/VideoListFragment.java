package de.uulm.fhenkel.livetube.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ListView;
import java.util.Map;
import de.uulm.fhenkel.livetube.R;
import de.uulm.fhenkel.livetube.adapters.VideoListAdapter;
import de.uulm.fhenkel.livetube.ddp.models.Video;
import de.uulm.fhenkel.livetube.youtube.YouTubeActivity;


public class VideoListFragment extends android.support.v4.app.Fragment {

    private final static String TAG = "VideoListFrag";

    private ListView listview;
    private VideoListAdapter videoList;
    private YouTubeActivity activity;
    private Map<String, Video> currentVideos;

    public VideoListFragment(VideoListAdapter videoList, YouTubeActivity activity) {
        this.videoList = videoList;
        this.activity = activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_video_list, container, false);
        final FrameLayout frameLayout = (FrameLayout) view.findViewById(R.id.fragment_videoList_FramedLayout);
        frameLayout.setBackgroundColor(0);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Instantiate the listview
        listview = (ListView) ((Activity)activity).findViewById(R.id.video_list);
        listview.setAdapter(videoList);
        listview.setEmptyView(((Activity) activity).findViewById(R.id.empty_video_list));


    }

    /**
     * Disables the functionality to change a video
     */
    public void disableVideoChange() {
        listview.setOnItemClickListener(null);
        listview.setOnLongClickListener(null);
    }

    /**
     * Disables the functionality to delete a video
     */
    public void disableVideoDelete() {
        listview.setOnLongClickListener(null);
    }

    /**
     * Enables the functionality to change a video
     */
    public void enableVideoChange() {
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Video v = videoList.getItem(position);
                activity.playVideo(v.getYTId());
            }
        });
    }

    /**
     * Enables the functionality to delete a video
     */
    public void enableVideoDelete() {
        listview.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            public boolean onItemLongClick(AdapterView<?> arg0, View arg1, int pos, long id) {
                showDeleteVideoDialog(pos);
                return true;
            }
        });
    }

    /**
     * Update the view if data changed
     */
    public void notifyDataChanged() {

        if(videoList != null) {
            videoList.notifyDataSetChanged();
        }

    }

    /**
     * Opens a confirmation dialog when a video is deleted
     *
     * @param position
     */
    public void showDeleteVideoDialog(final int position) {

        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:

                        // Remove the video and rerender the list
                        Video v = videoList.getItem(position);
                        activity.removeVideo(v.getYTId());
                        notifyDataChanged();

                        break;
                    case DialogInterface.BUTTON_NEGATIVE:
                        // No button clicked
                        // do nothing

                        break;
                }
            }
        };
        AlertDialog.Builder builder = new AlertDialog.Builder((Context)activity);
        builder.setMessage(((Context) activity).getString(R.string.delete_video_dialog))
                .setPositiveButton(((Context) activity).getString(R.string.positive_delete_dialog), dialogClickListener)
                .setNegativeButton(((Context) activity).getString(R.string.negative_delete_dialog), dialogClickListener)
                .show();
    }


}
