package de.uulm.fhenkel.livetube.ddp.models;

import android.util.Log;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by paulmohr on 12.05.15.
 */
public class Channel {

    /**
     * This is a reference to the hashmap object in our "data store"
     * so we can look up fields dynamically
     */
    private Map<String, Object> fields;
    /** This is our object ID */
    private String docId;

    /**
     * Constructor for Channel object
     */
    public Channel(String docId, Map<String, Object> fields) {
        this.fields = fields;
        this.docId = docId;
    }

    public String getSlug() {
        return (String)fields.get("slug");
    }

    /**
     * Returns the current status of the active video from this channel
     * Should only be -1, 0, 1 or 2
     * -1 (unstarted)
     * 0 (ended)
     * 1 (playing)
     * 2 (paused)
     * 3 (buffering)
     * 5 (video cued)
     *
     * @return
     */
    public int getStatus() {
        return ((Double)fields.get("currentStatus")).intValue();
    }

    /**
     * Returns the YouTube id of the active video
     *
     * @return
     */
    public String getVideoId() {
        return (String)fields.get("active");
    }

    public float getCurrentTime() {
        return ((Double)fields.get("currentTime")).floatValue();
    }

    public int getCurrentTimeUpdated() {
        return ((Double)fields.get("currentTimeUpdated")).intValue();
    }

    public String getOwner() {
        return (String)fields.get("owner");
    }
}
