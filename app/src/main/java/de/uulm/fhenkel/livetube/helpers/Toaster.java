package de.uulm.fhenkel.livetube.helpers;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.view.Gravity;
import android.view.View;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubePlayer;

public class Toaster {

    private View view;

    public Toaster(View view) {
        this.view = view;
    }

    public void showPredefinedErrorMessage (String message) {

        int duration = Toast.LENGTH_LONG;

        if(message.equals("NOT_PLAYABLE")) {
            message = "The current video could not be loaded because it is not in a playable state.";
        }
        if(message.equals("INTERNAL_ERROR")) {
            message = "The current video could not be loaded due to an internal error.";
        }
        if(message.equals("NETWORK_ERROR")) {
            message = "An error occurred due to a network request failing.";
        }
        if(message.equals("PLAYER_VIEW_NOT_VISIBLE")) {
            message = "Playback has been stopped due to the player's View not being visible.";
        }
        if(message.equals("PLAYER_VIEW_NOT_VISIBLE")) {
            message = "Playback has been stopped due to the player's View not being visible.";
        }
        if(message.equals("UNAUTHORIZED_OVERLAY")) {
            message = "Playback has been stopped due to a view overlaying the player.";
        }
        if(message.equals("UNKNOWN")) {
            message = "The reason for the error is not known.";
        }

        Snackbar.make(view, message, Snackbar.LENGTH_LONG).show();
    }

    public void showCustomText(String text) {
        Snackbar.make(view, text, Snackbar.LENGTH_LONG).show();
    }
}
