package de.uulm.fhenkel.livetube.helpers;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import de.uulm.fhenkel.livetube.ddp.MyDDPState;

public class CredentialsPreferencesHelper {

    private static final String SHARED_PREFS_LIVETUBE = "de.uulm.fhenkel.livetube";
    private static final String SHARED_PREFS_EMAIL = "de.uulm.fhenkel.livetube.email";
    private static final String SHARED_PREFS_PASSWORD = "de.uulm.fhenkel.livetube.password";
    private static final String SHARED_PREFS_LOGGED_IN = "de.uulm.fhenkel.livetube.logged.in";

    private static final String TAG = "CredentialsPreferences";

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private Context context;

    public CredentialsPreferencesHelper(Context context) {

        this.context = context;

        init();
    }

    public static String getSharedPrefsLivetube() {
        return SHARED_PREFS_LIVETUBE;
    }

    /**
     *
     */
    private void init() {
        sharedPreferences = context.getSharedPreferences(SHARED_PREFS_LIVETUBE, Activity.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }

    /**
     * Check if the login credentials are saved
     *
     * @return
     */
    public boolean checkIfUserWasLoggedIn() {

        return sharedPreferences.getBoolean(SHARED_PREFS_LOGGED_IN, false);

    }

    /**
     * Retrieves the credentials.
     *
     * @return The first element is the email, the second the password
     */
    public String[] getCredentials() {

        String[] credentials = new String[2];
        credentials[0] = sharedPreferences.getString(SHARED_PREFS_EMAIL, "");
        credentials[1] = sharedPreferences.getString(SHARED_PREFS_PASSWORD, "");

        return credentials;
    }

    /**
     * Erases the saved credentials and sets the state of the login to 'false'
     *
     */
    public void eraseCredentials() {

        boolean loggedIn = false;
        String emptyString = "";

        editor.putBoolean(SHARED_PREFS_LOGGED_IN, loggedIn);
        editor.putString(SHARED_PREFS_EMAIL, emptyString);
        editor.putString(SHARED_PREFS_PASSWORD, emptyString);
        editor.apply();

    }

    /**
     * Save the credentials and save them asynchronous
     *
     * @param email
     * @param password
     */
    public void saveCredentials(String email, String password) {

        boolean loggedIn = true;

        editor.putBoolean(SHARED_PREFS_LOGGED_IN, loggedIn);
        editor.putString(SHARED_PREFS_EMAIL, email);
        editor.putString(SHARED_PREFS_PASSWORD, password);
        editor.apply();

    }
}
