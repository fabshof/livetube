package de.uulm.fhenkel.livetube.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ListView;

import com.keysolutions.ddpclient.DDPListener;
import com.keysolutions.ddpclient.android.DDPBroadcastReceiver;
import com.keysolutions.ddpclient.android.DDPStateSingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import de.uulm.fhenkel.livetube.R;
import de.uulm.fhenkel.livetube.adapters.ChatListAdapter;
import de.uulm.fhenkel.livetube.ddp.MyDDPState;
import de.uulm.fhenkel.livetube.ddp.models.Channel;
import de.uulm.fhenkel.livetube.youtube.YouTubeActivity;

public class ChatFragment extends android.support.v4.app.Fragment {

    public static final String TAG = "ChatFragment";

    private ListView listView;
    private ChatListAdapter chatListAdapter;
    private DDPBroadcastReceiver ddpBroadcastReceiver;
    private YouTubeActivity youTubeActivity;
    private LinkedHashMap<String, String> messages;
    private LinkedHashMap<String, String> names;
    private String activeChannelSlug;
    private DDPStateSingleton ddp;
    private View view;

    public ChatFragment(YouTubeActivity youTubeActivity, String activeChannelSlug) {
        this.youTubeActivity = youTubeActivity;
        this.activeChannelSlug = activeChannelSlug;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_chat, container, false);
        final FrameLayout frameLayout = (FrameLayout) view.findViewById(R.id.fragment_chat_frameLayout);
        frameLayout.setBackgroundColor(0);

        ChatFragment.this.view = view;
        updateBroadcastReceiver();

        return view;
    }

    @Override
    public void onViewCreated(final View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);



        // Handle the enter event and save the message
        final EditText editText = (EditText)((Activity) youTubeActivity).findViewById(R.id.add_chat_text);
        editText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    switch (keyCode) {

                        case KeyEvent.KEYCODE_ENTER:

                            String contentToSave = editText.getText().toString();
                            if (!contentToSave.equals("")) {
                                ddp.call("addMessage", new Object[]{activeChannelSlug, contentToSave});
                                editText.setText("");
                                editText.clearFocus();
                                return true;
                            }
                            return true;

                        default:
                            break;
                    }
                }
                return false;
            }
        });

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        ddp = null;
        ddpBroadcastReceiver = null;
    }

    /**
     * Set the new active channel slug and update the chat list
     *
     * @param slug
     */
    public void setActiveChannelSlug(String slug) {

        activeChannelSlug = slug;
        updateBroadcastReceiver();

    }

    /**
     * Reinstantiate the broadcastreceiver
     */
    private void updateBroadcastReceiver() {

        ddpBroadcastReceiver = new DDPBroadcastReceiver(MyDDPState.getInstance(), (Activity)youTubeActivity) {

            @Override
            protected void onDDPConnect(DDPStateSingleton ddp) {
                super.onDDPConnect(ddp);

                ChatFragment.this.ddp = ddp;
                messages = new LinkedHashMap<>();
                names = new LinkedHashMap<>();

                ddp.call("getChannel", new Object[] {activeChannelSlug}, new DDPListener() {

                    @Override
                    public void onResult(Map<String, Object> resultFields) {
                        super.onResult(resultFields);

                        if(resultFields.get("msg").equals("result")) {

                            Map<String, Object> channelData = (Map<String, Object>) resultFields.get("result");
                            ArrayList<Map<String, Object>> entryData = (ArrayList<Map<String, Object>>)channelData.get("messages");

                            int counter = 0;

                            // The last name of the message owner
                            String tmpLastName = "";

                            if (entryData != null) {
                                for(Map<String, Object> entry: entryData) {

                                    String tmpName = entry.get("name").toString();
                                    String tmpMessage = entry.get("message").toString();

                                    // Check if the last message was from the same owner
                                    if (tmpLastName.equals(tmpName)) {
                                        tmpName = "";
                                    } else {
                                        tmpLastName = tmpName;
                                    }

                                    names.put(String.valueOf(counter), tmpName);
                                    messages.put(String.valueOf(counter), tmpMessage);

                                    counter++;
                                }
                            }

                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    // Instantiate the listview
                                    listView = (ListView) view.findViewById(R.id.message_list);
                                    chatListAdapter = new ChatListAdapter(messages, names);
                                    listView.setAdapter(chatListAdapter);
                                    listView.setOnItemClickListener(null);
                                    chatListAdapter.notifyDataSetChanged();
                                    listView.setEmptyView(((Activity) youTubeActivity).findViewById(R.id.empty_chat_list));
                                }
                            });


                        }

                    }
                });

            }

            @Override
            protected void onSubscriptionUpdate(String changeType, String subscriptionName, String docId) {
                super.onSubscriptionUpdate(changeType, subscriptionName, docId);

                if(subscriptionName.equals("channels")) {

                    if(ChatFragment.this.ddp != null) {

                        Map<String,Object> doc = ChatFragment.this.ddp.getCollection(subscriptionName).get(docId);
                        if (doc.get("slug").equals(activeChannelSlug)) {

                            ArrayList<Map<String,Object>> entryData = (ArrayList<Map<String,Object>>)doc.get("messages");

                            int counter = 0;

                            // Reset the hashmaps
                            names = new LinkedHashMap<String, String>();
                            messages = new LinkedHashMap<String, String>();

                            // The last name of the message owner
                            String tmpLastName = "";

                            if(entryData != null) {

                                // Fill the hashmaps again
                                for(Map<String, Object> entry: entryData) {

                                    String tmpName = entry.get("name").toString();
                                    String tmpMessage = entry.get("message").toString();

                                    // Check if the last message was from the same owner
                                    if (tmpLastName.equals(tmpName)) {
                                        tmpName = "";
                                    } else {
                                        tmpLastName = tmpName;
                                    }

                                    names.put(String.valueOf(counter), tmpName);
                                    messages.put(String.valueOf(counter), tmpMessage);

                                    counter++;
                                }
                            }

                            // Check if the activity still exists
                            if (getActivity() != null) {

                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        listView = (ListView) view.findViewById(R.id.message_list);
                                        chatListAdapter = new ChatListAdapter(messages, names);
                                        listView.setAdapter(chatListAdapter);
                                        listView.setOnItemClickListener(null);
                                        chatListAdapter.notifyDataSetChanged();
                                        listView.setSelection(chatListAdapter.getCount() - 1);
                                    }
                                });
                            }

                        }
                    }

                }
            }
        };
        MyDDPState.getInstance().connectIfNeeded();

    }

}
