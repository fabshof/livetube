package de.uulm.fhenkel.livetube.youtube;

import com.google.android.youtube.player.YouTubePlayerSupportFragment;

public class YouTubeFragment extends YouTubePlayerSupportFragment {

    public static YouTubeFragment newInstance(String developerKey, YouTubeController youTubeController) {

        YouTubeFragment playerYouTubeFrag = new YouTubeFragment();

        playerYouTubeFrag.initialize(developerKey, youTubeController);
        return playerYouTubeFrag;
    }
}