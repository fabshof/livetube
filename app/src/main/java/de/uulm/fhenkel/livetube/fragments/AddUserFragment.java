package de.uulm.fhenkel.livetube.fragments;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.keysolutions.ddpclient.android.DDPStateSingleton;

import de.uulm.fhenkel.livetube.R;
import de.uulm.fhenkel.livetube.activities.ChannelSettingsActivity;
import de.uulm.fhenkel.livetube.helpers.Toaster;

public class AddUserFragment extends DialogFragment {

    DDPStateSingleton ddp;

    public void setDDP(DDPStateSingleton ddp) {
        this.ddp = ddp;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        final View view = LayoutInflater.from(this.getActivity()).inflate(R.layout.dialog_adduser, null);
        // Set the view for the dialog
        builder.setView(view);

        // Add and handle ok and cancel buttons
        builder
                .setPositiveButton(R.string.add_user_action, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Bundle arguments = AddUserFragment.this.getArguments();

                        String username = ((TextView) view.findViewById(R.id.user_name)).getText().toString();
                        DDPStateSingleton ddp = ((ChannelSettingsActivity)getActivity()).getDDP();
                        if(ddp != null) {
                            ((ChannelSettingsActivity) getActivity()).getDDP().call("addChannelMember", new Object[]{arguments.getString("channelSlug"), username});
                        } else {
                            Toaster toaster = new Toaster(getActivity().findViewById(android.R.id.content));
                            toaster.showPredefinedErrorMessage("No server connection. Try again later.");
                        }
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });

        // Create the AlertDialog object and return it
        return builder.create();
    }
}
