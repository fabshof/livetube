package de.uulm.fhenkel.livetube.activities;

import android.content.BroadcastReceiver;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.keysolutions.ddpclient.DDPClient;
import com.keysolutions.ddpclient.android.DDPBroadcastReceiver;
import com.keysolutions.ddpclient.android.DDPStateSingleton;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import de.uulm.fhenkel.livetube.R;
import de.uulm.fhenkel.livetube.adapters.TopChannelAdapter;
import de.uulm.fhenkel.livetube.ddp.MyDDPState;
import de.uulm.fhenkel.livetube.ddp.models.Channel;
import de.uulm.fhenkel.livetube.ddp.models.Right;
import de.uulm.fhenkel.livetube.fragments.AddUserFragment;
import de.uulm.fhenkel.livetube.fragments.JoinChannelFragment;
import de.uulm.fhenkel.livetube.helpers.Helper;
import de.uulm.fhenkel.livetube.helpers.IntentHelper;
import de.uulm.fhenkel.livetube.helpers.Toaster;

public class TopChannelActivity extends AppCompatActivity {

    private BroadcastReceiver broadcastReceiver;
    private DDPStateSingleton ddp;
    private String channelSlug;
    private ArrayList<Channel> channels;
    private TopChannelAdapter adapter;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_top_channels);

        setTitle("Livetube - Top Channels");

        Log.e("foo", "CREATE");

        channels = new ArrayList<>();
        createTopChannels(channels);
        setupDDP();

        findViewById(R.id.join_channel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JoinChannelFragment addUserFragment = new JoinChannelFragment();
                FragmentManager fragmentManager = getSupportFragmentManager();
                addUserFragment.show(fragmentManager, "dialog");
            }
        });

    }


    private void createTopChannels(ArrayList<Channel> channels) {
        RecyclerView topChannelList = (RecyclerView) findViewById(R.id.top_channels);
        // use a linear layout manager
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        topChannelList.setLayoutManager(layoutManager);

        // specify an adapter (see also next example)
        adapter = new TopChannelAdapter(channels, this);

        topChannelList.setAdapter(adapter);
    }

    private void setupDDP() {
        Log.e("foo", "setupDDP ");
        if(ddp != null) {
            Log.e("foo","ddp is: " + ddp.getState());
        }
        broadcastReceiver = new DDPBroadcastReceiver(MyDDPState.getInstance(), this) {
            @Override
            protected void onDDPConnect(DDPStateSingleton ddp) {
                Log.e("foo","CONNECTED");
                super.onDDPConnect(ddp);
                TopChannelActivity.this.ddp = ddp;
                ddp.subscribe("topChannels", new Object[]{});
            }

            @Override
            protected void onSubscriptionUpdate(String changeType, String subscriptionName, String docId) {

                if(subscriptionName.equals("channels")) {

                    // Update right checkbox
                    if (changeType.equals(DDPClient.DdpMessageType.ADDED)) {
                        Log.e("foo","receive");
                        Map<String,Object> doc = ddp.getCollection(subscriptionName).get(docId);
                        Channel channel = new Channel(docId, doc);
                        channels.add(channel);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                adapter.notifyDataSetChanged();
                            }
                        });
                    }
                }

            }

            @Override
            protected void onError(String title, String msg) {
                //super.onError(title, msg);
                Log.e("foo","ERROR: " + msg);
            }
        };
        MyDDPState.getInstance().connectIfNeeded();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(ddp != null) {
            Log.e("foo","DISCONNECT NOW");
            MyDDPState.getInstance().unsubscribe("topChannels");
            broadcastReceiver = null;
            MyDDPState.getInstance().disconnect();
            MyDDPState.getInstance().clearObservers();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == IntentHelper.TOP_CHANNELS_REQUEST_CODE) {

            // An error occured
            if (resultCode == RESULT_CANCELED && data != null) {

                if (data.hasExtra(getString(R.string.channel_return_insufficient_permissions))) {
                    String errorMessage = data.getStringExtra(getString(R.string.channel_return_insufficient_permissions));
                    Toaster toaster = new Toaster(findViewById(android.R.id.content));
                    toaster.showCustomText(errorMessage);
                }

            }
        }
    }
}
