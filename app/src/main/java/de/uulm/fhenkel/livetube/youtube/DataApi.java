package de.uulm.fhenkel.livetube.youtube;

import android.util.Log;

import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.model.ResourceId;
import com.google.api.services.youtube.model.SearchListResponse;
import com.google.api.services.youtube.model.SearchResult;
import com.google.api.services.youtube.model.Thumbnail;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import de.uulm.fhenkel.livetube.util.Config;

public class DataApi {

    YouTube youtube;

    public DataApi() {

        // This object is used to make YouTube Data API requests. The last
        // argument is required, but since we don't need anything
        // initialized when the HttpRequest is initialized, we override
        // the interface and provide a no-op function.
        youtube = new YouTube.Builder(new NetHttpTransport(), new JacksonFactory(), new HttpRequestInitializer() {
            public void initialize(HttpRequest request) throws IOException {
            }
        }).setApplicationName("livetube-android").build();
    }

    /**
     * Query the YouTube data API
     *
     * @param query
     * @return
     */
    public List<SearchResult> search(String query) {

        try {

            // Define the API request for retrieving search results.
            YouTube.Search.List search = youtube.search().list("id,snippet");

            String apiKey = Config.DEVELOPER_KEY;
            search.setKey(apiKey);
            search.setQ(query);

            // Restrict the search results to only include videos. See:
            // https://developers.google.com/youtube/v3/docs/search/list#type
            search.setType("video");

            // To increase efficiency, only retrieve the fields that the
            // application uses.
            search.setFields("items(id/videoId,snippet/title,snippet/thumbnails/default/url)");
            search.setMaxResults(5l);

            // Call the API and print results.
            SearchListResponse searchResponse = search.execute();
            prettyPrint(searchResponse.getItems().iterator());

            return searchResponse.getItems();

        } catch (GoogleJsonResponseException e) {
            Log.e("yt-data-api", "There was a service error: " + e.getDetails().getCode() + " : " + e.getDetails().getMessage());
        } catch (IOException e) {
            Log.e("yt-data-api", "There was an IO error: " + e.getCause() + " : " + e.getMessage());
        }
        Log.e("yt-data-api","done");
        return null;
    }

    private static void prettyPrint(Iterator<SearchResult> iteratorSearchResults) {

        Log.e("youtuberesult", "\n=============================================================");
        Log.e("youtuberesult", "=============================================================\n");

        if (!iteratorSearchResults.hasNext()) {
            Log.e("youtuberesult", " There aren't any results for your query.");
        }

        while (iteratorSearchResults.hasNext()) {

            SearchResult singleVideo = iteratorSearchResults.next();
            ResourceId rId = singleVideo.getId();

            Thumbnail thumbnail = singleVideo.getSnippet().getThumbnails().getDefault();

            Log.e("youtuberesult"," Video Id" + rId.getVideoId());
            Log.e("youtuberesult", " Title: " + singleVideo.getSnippet().getTitle());
            Log.e("youtuberesult", " Thumbnail: " + thumbnail.getUrl());
            Log.e("youtuberesult", "\n-------------------------------------------------------------\n");
        }
    }
}
