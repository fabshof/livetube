package de.uulm.fhenkel.livetube.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;

import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.keysolutions.ddpclient.DDPClient;
import com.keysolutions.ddpclient.DDPListener;
import com.keysolutions.ddpclient.EmailAuth;
import com.keysolutions.ddpclient.android.DDPBroadcastReceiver;
import com.keysolutions.ddpclient.android.DDPStateSingleton;

import java.util.Formatter;
import java.util.Map;

import de.uulm.fhenkel.livetube.MyApplication;
import de.uulm.fhenkel.livetube.R;
import de.uulm.fhenkel.livetube.ddp.MyDDPState;
import de.uulm.fhenkel.livetube.helpers.CredentialsPreferencesHelper;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends ActionBarActivity {


    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */
    private UserLoginTask mAuthTask = null;

    private static final String TAG = "LoginActivity";
    private static final int AUTOCOMPLETE_THRESHOLD = 1;

    private DDPBroadcastReceiver broadcastReceiver;
    private DDPStateSingleton ddp;
    private BroadcastReceiver mReceiver;
    private CredentialsPreferencesHelper helper;
    private LoginActivity loginActivity = this;
    private Context context = this;
    private boolean readyForNewAttempt = true;
    private boolean userIsLoggedIn = false;

    // UI references.
    private AutoCompleteTextView mEmailView;
    private EditText mPasswordView;
    private TextView mInfoText;
    private View mProgressView;
    private View mLoginFormView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // Check if the user is logged in
        userIsLoggedIn = checkIfUserIsLoggedIn();

         helper = new CredentialsPreferencesHelper(this);

        // Instantiate the autoCompleteTextView and enable the hints for email adresses
        mEmailView = (AutoCompleteTextView) findViewById(R.id.email);
        String[] emailArray = getEmailArray();
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, emailArray);
        mEmailView.setThreshold(AUTOCOMPLETE_THRESHOLD);
        mEmailView.setAdapter(adapter);

        mInfoText = (TextView)findViewById(R.id.login_info_text);

        // Listen for the enter button press
        mPasswordView = (EditText) findViewById(R.id.password);
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        // Instantiate the actionButton
        Button emailActionButton = (Button) findViewById(R.id.email_action_button);
        emailActionButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!userIsLoggedIn) {
                    attemptLogin();
                } else {
                    attemptLogout();
                }
            }
        });

        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);

        // Hide the login elements if the user is logged in, change the text on the actionbutton and show an infotext
        if (userIsLoggedIn) {

            mPasswordView.setVisibility(View.GONE);
            mEmailView.setVisibility(View.GONE);

            mInfoText.setVisibility(View.VISIBLE);
            String username = MyApplication.getUserName();
            String textToDisplay = getString(R.string.already_logged_in);
            Formatter formatter = new Formatter();
            mInfoText.setText(formatter.format(textToDisplay, username).toString());

            emailActionButton.setText(getText(R.string.action_logout));

        }

    }

    /**
     * Populate the autocomplete propositions with the successfully used emails
     */
    private String[] getEmailArray() {

        SharedPreferences sharedPreferences = getPreferences(MODE_PRIVATE);
        String emails = sharedPreferences.getString(getString(R.string.shared_prefs_emails), "");

        String[] items;

        // If there is more than one value in the email string separated by ";"
        if (emails.contains(";")) {

            items = emails.split(";");

        // If there is only one address in the sharedPreferences
        } else if (!emails.equals("")) {

            items = new String[] {};

        } else {

            items = new String[] {};

        }

        return items;
    }

    /**
     * Add the new email to the already saved emails
     *
     * @param email
     */
    private void addEmailToAutoComplete(String email) {

        SharedPreferences sharedPreferences = getPreferences(MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        // Check if there are already some emails saved
        String oldEmails = sharedPreferences.getString(getString(R.string.shared_prefs_emails), "");

        if (oldEmails.equals("")) {
            oldEmails = email;
        } else {
            // Check if the email was not saved before
            if(checkDistinct(oldEmails, email)) {
                oldEmails += ";" + email;
            }
        }

        editor.putString(getString(R.string.shared_prefs_emails), oldEmails);
        editor.commit();

    }

    /**
     * Check if the email already exists in the list of proposals
     *
     * @param emails
     * @param email
     * @return
     */
    private boolean checkDistinct(String emails, String email) {

        // If the email is the first
        if (emails.equals("")) {
            return true;
        }

        // If there is more than one email
        if (emails.contains(";")) {

            // Split the emails string at the delimiters
            String[] items = emails.split(";");

            // Go through all emails and check if the entry already exists
            for (String item: items) {
                if (item.equals(email)) {
                    return false;
                }
            }

            return true;

        } else {

            return !email.equals(emails);

        }

    }

    /**
     * Check if the user is logged in
     *
     * @return
     */
    private boolean checkIfUserIsLoggedIn() {

        LoginActivity.this.ddp = MyDDPState.getInstance();

        return LoginActivity.this.ddp.isLoggedIn();

    }


    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    public void attemptLogin() {

        if (!readyForNewAttempt) {
            return;
        }

        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        } else if (!isEmailValid(email)) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }

        // Check if anything canceled the login attempt
        if (cancel) {

            focusView.requestFocus();
            readyForNewAttempt = true;

        } else {

            showProgress(true);
            mAuthTask = new UserLoginTask(email, password, helper);
            mAuthTask.execute();

        }
    }

    /**
     * Attempt to log the user out
     */
    public void attemptLogout() {

        showProgress(true);
        String email = mEmailView.getText().toString();

        UserLogoutTask logoutTask = new UserLogoutTask(email, helper);
        logoutTask.execute();

    }

    private boolean isEmailValid(String email) {
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        return password.length() > 4;
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    public void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    public class UserLoginTask extends AsyncTask<String, String, String> {

        private final String mEmail;
        private final String mPassword;
        private CredentialsPreferencesHelper mHelper;

        UserLoginTask(String email, String password, CredentialsPreferencesHelper helper) {
            mEmail = email;
            mPassword = password;
            mHelper = helper;
        }

        @Override
        protected String doInBackground(String... params) {

            broadcastReceiver = new DDPBroadcastReceiver(MyDDPState.getInstance(), loginActivity) {

                @Override
                protected void onDDPConnect(final DDPStateSingleton ddp) {
                    super.onDDPConnect(ddp);

                    LoginActivity.this.ddp = ddp;

                    Object[] methodArgs = new Object[1];
                    EmailAuth userpass = new EmailAuth(mEmail, mPassword);
                    methodArgs[0] = userpass;

                    ddp.call("login", methodArgs, new DDPListener(){
                        public void onResult(Map<String, Object> jsonFields) {

                            // Hide the progress rotator
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    showProgress(false);
                                }
                            });

                            // If the server response has a result
                            if (jsonFields.containsKey("result")) {

                                Map<String, Object> result = (Map<String, Object>) jsonFields.get(DDPClient.DdpMessageField.RESULT);

                                // Save the email to the autocomplete proposals and save the credentials
                                addEmailToAutoComplete(mEmail);
                                mHelper.saveCredentials(mEmail, mPassword);

                                // Tell the application which user is logged in
                                MyApplication.setUserId((String)result.get("id"));

                                // Create the data returned to the main activity
                                        ((MyDDPState) ddp).setDDPState(DDPStateSingleton.DDPSTATE.LoggedIn);

                                // Finish the subactivity and return the successful login result
                                Intent returnIntent = new Intent();
                                LoginActivity.this.setResult(RESULT_OK, returnIntent);
                                finish();

                            } else if (jsonFields.containsKey("error")) {

                                // get the error message
                                Map<String, Object> error = (Map<String, Object>) jsonFields.get(DDPClient.DdpMessageField.ERROR);

                                // get the float error reason and make a string without the point out of it
                                String reason =  (String.valueOf(error.get("error"))).substring(0, 3);

                                String errorToDisplay = "";
                                // User is not authorized
                                if (reason.equals("403")) {
                                    errorToDisplay = getResources().getString(R.string.error_incorrect_password);
                                } else {
                                    errorToDisplay = getResources().getString(R.string.error_test_your_input);
                                }

                                final String finalError = errorToDisplay;
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        mPasswordView.setError(finalError);
                                        mPasswordView.requestFocus();
                                    }
                                });

                                readyForNewAttempt = true;

                                }

                        }
                    });
                }
            };
            MyDDPState.getInstance().connectIfNeeded();

            return "";
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            showProgress(false);
        }
    }

    public class UserLogoutTask extends AsyncTask<String, String, String> {

        private final String mEmail;
        private CredentialsPreferencesHelper mHelper;

        UserLogoutTask(String email, CredentialsPreferencesHelper helper) {
            mEmail = email;
            mHelper = helper;
        }

        @Override
        protected String doInBackground(String... params) {

            broadcastReceiver = new DDPBroadcastReceiver(MyDDPState.getInstance(), loginActivity) {

                @Override
                protected void onDDPConnect(DDPStateSingleton ddp) {
                    super.onDDPConnect(ddp);

                    LoginActivity.this.ddp = ddp;

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            showProgress(true);
                        }
                    });

                    if (ddp.isLoggedIn()) {
                        ddp.logout();
                    }

                    helper.eraseCredentials();

                    Intent returnIntent = new Intent();
                    LoginActivity.this.setResult(RESULT_OK, returnIntent);
                    finish();
                }
            };
            MyDDPState.getInstance().connectIfNeeded();

            return "";
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            showProgress(false);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        // If the back key is pressed and the activity is left like that
        if (keyCode == KeyEvent.KEYCODE_BACK ) {

            // Hide the progress rotator
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showProgress(false);
                }
            });

            Intent returnIntent = new Intent();
            LoginActivity.this.setResult(RESULT_CANCELED, returnIntent);
            finish();

        }

        return super.onKeyDown(keyCode, event);
    }
}



