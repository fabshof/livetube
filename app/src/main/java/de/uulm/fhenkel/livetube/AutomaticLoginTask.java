package de.uulm.fhenkel.livetube;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.keysolutions.ddpclient.DDPClient;
import com.keysolutions.ddpclient.DDPListener;
import com.keysolutions.ddpclient.EmailAuth;
import com.keysolutions.ddpclient.android.DDPBroadcastReceiver;
import com.keysolutions.ddpclient.android.DDPStateSingleton;

import java.util.Map;
import java.util.Objects;

import de.uulm.fhenkel.livetube.activities.LoginActivity;
import de.uulm.fhenkel.livetube.ddp.MyDDPBroadcastReceiver;
import de.uulm.fhenkel.livetube.ddp.MyDDPState;

public class AutomaticLoginTask extends AsyncTask<String, String, String> {

    public final static String TAG = "AutomaticLoginTask";

    private final String mEmail;
    private final String mPassword;
    private final Context context;

    AutomaticLoginTask(String email, String password, Context context) {
        mEmail = email;
        mPassword = password;
        this.context = context;
    }

    @Override
    protected String doInBackground(String... params) {

        MyDDPBroadcastReceiver broadcastReceiver = new MyDDPBroadcastReceiver(MyDDPState.getInstance(), context) {

            @Override
            protected void onDDPConnect(final DDPStateSingleton ddp) {
                super.onDDPConnect(ddp);

                Object[] methodArgs = new Object[1];
                EmailAuth userpass = new EmailAuth(mEmail, mPassword);
                methodArgs[0] = userpass;

                ddp.call("login", methodArgs, new DDPListener(){
                    public void onResult(Map<String, Object> jsonFields) {

                        // If the server response has a result
                        if (jsonFields.containsKey("result")) {

                            Map<String, Object> result = (Map<String, Object>) jsonFields.get(DDPClient.DdpMessageField.RESULT);

                            // Create the data returned to the main activity
                            ((MyDDPState) ddp).setDDPState(DDPStateSingleton.DDPSTATE.LoggedIn);

                            // Retrieve the username by userId
                            String userId = (String)result.get("id");
                            Map<String, Object> userFields = MyDDPState.getInstance().getDocument("users", userId);
                            MyApplication.setUserName((String)userFields.get("username"));
                            MyApplication.setUserId(userId);


                        } else if (jsonFields.containsKey("error")) {

                        }

                    }
                });
            }
        };
        MyDDPState.getInstance().connectIfNeeded();

        return "";
    }

    @Override
    protected void onCancelled() {

    }
}
