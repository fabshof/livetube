package de.uulm.fhenkel.livetube.ddp.models;

import android.util.Log;

import java.util.Map;

/**
 * Created by paulmohr on 12.05.15.
 */
public class Video {

    /**
     * This is a reference to the hashmap object in our "data store"
     * so we can look up fields dynamically
     */
    private Map<String, Object> fields;
    /** This is our object ID */
    private String docId;
    private int order = 0;

    /**
     * Constructor for Video object
     */
    public Video(String docId, Map<String, Object> fields) {
        this.fields = fields;
        this.docId = docId;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public String getTitle() {
        return (String)this.fields.get("title");
    }
    public String getYTId() {
        return (String)this.fields.get("ytid");
    }
    public String getChannelSlug() {
        return (String)this.fields.get("channel");
    }
    public int getOrder() {
        return order;
    }
    public String getDuration() {
        return (String)this.fields.get("duration");
    }
}
