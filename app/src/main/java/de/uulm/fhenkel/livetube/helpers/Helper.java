package de.uulm.fhenkel.livetube.helpers;

import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ImageView;

import java.util.concurrent.TimeUnit;

public class Helper {

    /**
     * This method makes a superawesome looking String out of the time given in milliseconds
     *
     * @param timeMillis
     * @return
     */
    public static String getSuperAwesomeTimeDisplay(long timeMillis) {

        String result = String.format("%02d : %02d",
                TimeUnit.MILLISECONDS.toMinutes(timeMillis),
                TimeUnit.MILLISECONDS.toSeconds(timeMillis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(timeMillis))
        );

        return result;
    }

    /**
     * The method parses the time responded by the server to a well looking format
     *
     * @param rawTime
     * @return
     */
    public static String parseISOtoAwesomeTime(String rawTime) {

        // If there is no time input, return a string with zero time
        if(rawTime == null || rawTime == "") {
            return "00:00:00";
        }

        // Replace the start delimiter chars
        int charPosAfterStartDelimiter = 2;
        String formattedTime = "";
        rawTime = rawTime.substring(charPosAfterStartDelimiter);

        // Go from left to right through the retrieved String and get the hours, minutes and seconds out of it
        String[] componentsH;
        if(rawTime.contains("H")) {
            componentsH = rawTime.split("H");
        } else {
            componentsH = new String[2];
            componentsH[0] = "00";
            componentsH[1] = rawTime;
        }

        String[] componentsM;
        if (componentsH[1].contains("M")) {
            componentsM = componentsH[1].split("M");
        } else {
            componentsM = new String[2];
            componentsM[0] = "00";
            componentsM[1] = componentsH[1];
        }

        String[] componentsS = new String[1];
        if(componentsM.length == 2) {
            if(componentsM[1].contains("S")) {
                componentsS[0] = componentsM[1].replace("S", "");
            } else {
                componentsS[0] = "00";
            }
        } else {
            componentsS[0] = "00";
        }


        // Add the retrieved values to the String.
        formattedTime = componentsH[0] + ":" + componentsM[0] + ":" + componentsS[0];

        // Fill up the string parts to have a length of two chars
        String[] strings = formattedTime.split(":");

        int h, m, s;

        if(strings[0] != null || strings[0] != "") {
            h = Integer.parseInt(strings[0]);
        } else {
            h = 0;
        }

        if(strings[1] != null || strings[1] != "") {
            m = Integer.parseInt(strings[1]);
        } else {
            m = 0;
        }

        if(strings[2] != null || strings[2] != "") {
            s = Integer.parseInt(strings[2]);
        } else {
            s = 0;
        }

        String result = String.format("%02d:%02d:%02d", h, m, s);

        return result;

    }
}
