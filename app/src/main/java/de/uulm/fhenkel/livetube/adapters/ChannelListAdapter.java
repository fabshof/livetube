package de.uulm.fhenkel.livetube.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.Map;

import de.uulm.fhenkel.livetube.R;
import de.uulm.fhenkel.livetube.activities.MainActivity;
import de.uulm.fhenkel.livetube.ddp.models.Channel;

public class ChannelListAdapter extends RecyclerView.Adapter<ChannelListAdapter.ViewHolder> {

    private static final int TYPE_HEADER = 0;  // Declaring Variable to Understand which View is being worked on
    // IF the view under inflation and population is header or Item
    private static final int TYPE_ITEM = 1;

    private Map<String, Channel> topChannels;
    private AdapterView.OnItemClickListener topChannelClickListener;
    private static TopChannelListAdapter topChannelListAdapted;


    public ChannelListAdapter(Map<String, Channel> topChannels, ListView.OnItemClickListener topChannelClickListener) {
        this.topChannels = topChannels;
        this.topChannelClickListener = topChannelClickListener;
    }

    // Creating a ViewHolder which extends the RecyclerView View Holder
    // ViewHolder are used to to store the inflated views in order to recycle them
    public static class ViewHolder extends RecyclerView.ViewHolder {
        int holderId;

        TextView userNameView;

        ListView topChannelList;


        public ViewHolder(View itemView, int ViewType) {
            super(itemView);


            // Here we set the appropriate view in accordance with the the view type as passed when the holder object is created
            if (ViewType == TYPE_ITEM) {
                holderId = 1;
                topChannelList = (ListView) itemView.findViewById(R.id.top_channel_list);
            } else {
                userNameView = (TextView) itemView.findViewById(R.id.user_name);
                holderId = 0;
            }
        }
    }


    //Below first we override the method onCreateViewHolder which is called when the ViewHolder is
    //Created, In this method we inflate the item_row.xml layout if the viewType is Type_ITEM or else we inflate header.xml
    // if the viewType is TYPE_HEADER
    // and pass it to the view holder

    @Override
    public ChannelListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == TYPE_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.channel_list_contents, parent, false); //Inflating the layout

            return new ViewHolder(v, viewType); //Creating ViewHolder and passing the object of type view

        } else if (viewType == TYPE_HEADER) {

            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.channel_list_header, parent, false); //Inflating the layout

            return new ViewHolder(v, viewType); //Creating ViewHolder and passing the object of type view

        }
        return null;

    }

    //Next we override a method which is called when the item in a row is needed to be displayed, here the int position
    // Tells us item at which position is being constructed to be displayed and the holder id of the holder object tell us
    // which view type is being created 1 for item row
    @Override
    public void onBindViewHolder(ChannelListAdapter.ViewHolder holder, int position) {
        if (holder.holderId == 1) {
            // Top channel list
            topChannelListAdapted = new TopChannelListAdapter(topChannels);
            holder.topChannelList.setAdapter(topChannelListAdapted);
            holder.topChannelList.setOnItemClickListener(this.topChannelClickListener);
        } else {
            // Header
            holder.userNameView.setText("Paul Mohr");
        }
    }

    // This method returns the number of items present in the list
    @Override
    public int getItemCount() {
        // The header and the topChannel list
        return 2;
    }

    public Channel getTopChannel(int position) {
        return (Channel)topChannels.values().toArray()[position];
    }

    // With the following method we check what type of view is being passed
    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position)) {
            return TYPE_HEADER;
        }

        return TYPE_ITEM;
    }

    private boolean isPositionHeader(int position) {
        return position == 0;
    }

    public void notifyTopChannelList() {
        topChannelListAdapted.notifyDataSetChanged();
    }



}