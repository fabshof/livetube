package de.uulm.fhenkel.livetube.youtube;

import com.keysolutions.ddpclient.android.DDPStateSingleton;

public interface YouTubeActivity {

    public void youTubeInitFinished();
    public DDPStateSingleton getDdp();
    public String getActiveChannelSlug();
    public String getNextVideoYTId();
    public int getCurrentVideoPosition();
    public void rotatePlayerStateImage();
    public void haltPlayerStateImage();
    public boolean getIsYTPlayerReady();
    public void playVideo(String ytId);
    public void removeVideo(String ytId);

}
