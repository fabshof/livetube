package de.uulm.fhenkel.livetube.adapters;

import android.content.ClipData;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.api.services.youtube.model.SearchResult;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import de.uulm.fhenkel.livetube.R;
import de.uulm.fhenkel.livetube.ddp.models.Video;

public class YouTubeSearchResultsAdapter extends BaseAdapter {

    private List<SearchResult> data;

    public YouTubeSearchResultsAdapter(List<SearchResult> results) {
        data = results;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public SearchResult getItem(int position) {
        return (SearchResult)data.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO: Return a real id
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(parent.getContext());
            v = vi.inflate(R.layout.adapter_youtube_search_result, parent, false);
        }

        SearchResult result = getItem(position);

        if (result != null) {
            TextView title = (TextView) v.findViewById(R.id.video_name);
            ImageView thumbnail = (ImageView) v.findViewById(R.id.video_thumbnail);

            if (title != null) {
                title.setText(result.getSnippet().getTitle());
            }
            if(thumbnail != null) {
                ImageLoader imageLoader = ImageLoader.getInstance();
                imageLoader.displayImage(result.getSnippet().getThumbnails().getDefault().getUrl(), thumbnail);
            }

        }

        return v;
    }

}