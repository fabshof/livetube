package de.uulm.fhenkel.livetube.adapters;

import android.content.ClipData;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.lang.reflect.Array;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import javax.xml.datatype.Duration;

import de.uulm.fhenkel.livetube.R;
import de.uulm.fhenkel.livetube.ddp.models.Video;
import de.uulm.fhenkel.livetube.helpers.Helper;

public class VideoListAdapter extends BaseAdapter {

    private Map<String, Video> data;
    private int backgroundActivePosition;

    public VideoListAdapter(Map<String, Video> map) {
        data = map;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Video getItem(int position) {
        return getItems()[position];
    }

    private Video[] getItems() {
        Video[] videos = new Video[data.size()];
        int i = 0;
        Object[] objects = data.values().toArray();
        for(Object v : objects) {
            videos[i] = (Video)v;
            i++;
        }

        class VideoComparator implements Comparator<Video> {
            @Override
            public int compare(Video v1, Video v2) {
                if(v1.getOrder() > v2.getOrder()) {
                    return 1;
                }
                return -1;
            }
        }

        Arrays.sort(videos,new VideoComparator());

        return videos;
    }

    @Override
    public long getItemId(int position) {
        // TODO: Return a real id
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(parent.getContext());
            v = vi.inflate(R.layout.adapter_video_list, parent, false);
        }

        if(backgroundActivePosition == position) {
            v.setBackgroundResource(R.color.listview_selected_item);
        } else {
            v.setBackgroundResource(0);
        }

        Video p = getItem(position);

        if (p != null) {
            TextView title = (TextView) v.findViewById(R.id.video_title);
            TextView duration = (TextView) v.findViewById(R.id.video_duration);

            if (title != null) {
                title.setText(p.getTitle());
            }
            if (duration != null) {
                duration.setText(Helper.parseISOtoAwesomeTime(p.getDuration()));
            }
        }

        return v;
    }

    /**
     * The method returns the position of the currently active video in the listview
     *
     * @param ytId
     * @return
     */
    public int getCurrentVideoListPosition(String ytId) {

        Video[] videos = this.getItems();
        for (int i = 0; i < videos.length; i++) {

            Video video = videos[i];

            if(video.getYTId().equals(ytId)) {
                return i;
            }
        }

        return -1;

    }

    public void setBackgroundActive(int position) {
        backgroundActivePosition = position;
    }

}