package de.uulm.fhenkel.livetube.activities;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.keysolutions.ddpclient.DDPClient;
import com.keysolutions.ddpclient.DDPListener;
import com.keysolutions.ddpclient.android.DDPBroadcastReceiver;
import com.keysolutions.ddpclient.android.DDPStateSingleton;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import de.uulm.fhenkel.livetube.R;
import de.uulm.fhenkel.livetube.ddp.MyDDPState;
import de.uulm.fhenkel.livetube.ddp.models.Channel;
import de.uulm.fhenkel.livetube.ddp.models.Right;
import de.uulm.fhenkel.livetube.ddp.models.Video;
import de.uulm.fhenkel.livetube.fragments.AddUserFragment;

public class ChannelSettingsActivity extends AppCompatActivity {

    private BroadcastReceiver broadcastReceiver;
    private DDPStateSingleton ddp;
    private String channelSlug;

    private HashMap<String, CheckBox> checkBoxes = new HashMap<String, CheckBox>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_channel_settings);

        channelSlug = getIntent().getStringExtra("slug");

        CollapsingToolbarLayout toolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        toolbarLayout.setTitle(channelSlug);

        // Setup the various parts of this activity
        setupCheckboxes();

        setupDDP();

        // This needs to be called after setupDDP
        //setupAddUser();
    }

    private void setupCheckboxes() {
        Integer[] containers = new Integer[3];
        containers[0] = R.id.rights_guests;
        containers[1] = R.id.rights_members;
        containers[2] = R.id.rights_moderators;

        String[] containerNames = new String[3];
        containerNames[0] = "guest";
        containerNames[1] = "member";
        containerNames[2] = "moderator";

        String[] rights = new String[5];
        rights[0] = "viewChannel";
        rights[1] = "addVideo";
        rights[2] = "removeVideo";
        rights[3] = "changeActiveVideo";
        rights[4] = "addMessage";

        String[] rightLabels = new String[5];
        rightLabels[0] = "View and listen to channel";
        rightLabels[1] = "Add new videos";
        rightLabels[2] = "Delete videos";
        rightLabels[3] = "Set the active video and start / pause playback";
        rightLabels[4] = "Use channel chat";

        for (int i1 = 0; i1 < containers.length; i1++) {
            Integer container = containers[i1];
            LinearLayout containerLayout = (LinearLayout) findViewById(container);
            for (int i = 0; i < rights.length; i++) {
                CheckBox checkBox = new CheckBox(this);
                checkBox.setTag(R.id.tag_level,containerNames[i1]);
                checkBox.setTag(R.id.tag_right,rights[i]);
                checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        ddp.call("setRight",new Object[]{channelSlug,buttonView.getTag(R.id.tag_level),buttonView.getTag(R.id.tag_right),buttonView.isChecked()});
                    }
                });

                // Create a reference to the created checkbox so we can access it later based on level and right
                checkBoxes.put(containerNames[i1] + "-" + rights[i], checkBox);

                TextView checkBoxLabel = new TextView(this);
                checkBoxLabel.setText(rightLabels[i]);

                LinearLayout checkboxContainer = new LinearLayout(this);
                checkboxContainer.addView(checkBox);
                checkboxContainer.addView(checkBoxLabel);

                containerLayout.addView(checkboxContainer);
            }
        }
    }

    private void setupDDP() {
        broadcastReceiver = new DDPBroadcastReceiver(MyDDPState.getInstance(), this) {
            @Override
            protected void onDDPConnect(DDPStateSingleton ddp) {
                super.onDDPConnect(ddp);
                ChannelSettingsActivity.this.ddp = ddp;
                ddp.subscribe("channelRights", new Object[]{ChannelSettingsActivity.this.channelSlug});
            }

            @Override
            protected void onSubscriptionUpdate(String changeType, String subscriptionName, String docId) {

                if(subscriptionName.equals("rights")) {

                    // Update right checkbox
                    if (changeType.equals(DDPClient.DdpMessageType.ADDED) || changeType.equals(DDPClient.DdpMessageType.UPDATED) || changeType.equals(DDPClient.DdpMessageType.CHANGED)) {

                        Map<String,Object> doc = ddp.getCollection(subscriptionName).get(docId);
                        Right r = new Right(docId, doc);
                        CheckBox checkBox = checkBoxes.get(r.getLevel() + "-" + r.getRight());
                        checkBox.setChecked(r.getValue());
                    }
                }

            }

            @Override
            protected void onError(String title, String msg) {
                super.onError(title, msg);
            }
        };
        MyDDPState.getInstance().connectIfNeeded();
    }

    private void setupAddUser() {
        AddUserFragment addUserFragment = new AddUserFragment();
        Bundle bundle = new Bundle();
        bundle.putString("channelSlug", channelSlug);
        addUserFragment.setArguments(bundle);
        FragmentManager fragmentManager = getSupportFragmentManager();
        addUserFragment.show(fragmentManager,"dialog");
    }

    public DDPStateSingleton getDDP() {
        return ddp;
    }

}
