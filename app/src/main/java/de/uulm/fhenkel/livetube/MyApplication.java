package de.uulm.fhenkel.livetube;

import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;

import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import de.uulm.fhenkel.livetube.ddp.MyDDPState;
import de.uulm.fhenkel.livetube.helpers.CredentialsPreferencesHelper;
import de.uulm.fhenkel.livetube.util.Config;
import de.uulm.fhenkel.livetube.youtube.DataApi;
import de.uulm.fhenkel.livetube.youtube.YouTubeActivity;
import de.uulm.fhenkel.livetube.youtube.YouTubeController;
import de.uulm.fhenkel.livetube.youtube.YouTubeFragment;

public class MyApplication extends Application {
    /** Android application context */
    private static Context context = null;

    // Instance of our YouTube data API
    private static DataApi dataApi = null;

    private static String userId;
    private static String userName = "";

    private static YouTubeController youTubeController;
    private static YouTubeFragment youTubeFragment;

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        MyApplication.context = getApplicationContext();

        // Initialize the singletons so their instances are bound to the application process.
        initSingletons();
        initImageCache();
        login();
    }

    private void initImageCache() {
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)
                .memoryCacheExtraOptions(480, 800) // default = device screen dimensions
                .diskCacheExtraOptions(480, 800, null)
                .denyCacheImageMultipleSizesInMemory()
                .memoryCache(new LruMemoryCache(2 * 1024 * 1024))
                .memoryCacheSize(2 * 1024 * 1024)
                .diskCacheSize(50 * 1024 * 1024)
                .diskCacheFileCount(100)
            .build();
        ImageLoader.getInstance().init(config);
    }

    /**
     * The method first checks if the user was logged in and now needs to be logged in again.
     * Afterwards the login credentials are fetched and the user is tried to be logged in
     */
    private void login() {

        CredentialsPreferencesHelper helper = new CredentialsPreferencesHelper(context);

        if (helper.checkIfUserWasLoggedIn()) {

            String[] credentials = helper.getCredentials();
            attemptLogin(credentials[0], credentials[1]);

        }
    }

    /**
     * Attempt to login the user with the input data. No errors are displayed
     */
    public void attemptLogin(String email, String password) {

        AutomaticLoginTask automaticLoginTask = new AutomaticLoginTask(email, password, context);
        automaticLoginTask.execute();

    }

    /**
     * Initializes any singleton classes
     */
    protected void initSingletons() {
        // Initialize App DDP State Singleton
        MyDDPState.initInstance(this.getApplicationContext());
    }

    /**
     * Gets application context
     * @return Android application context
     */
    public static Context getAppContext() {
        return MyApplication.context;
    }

    /**
     * Creates and / or returns our YouTube data API singleton
     *
     * @return
     */
    public static DataApi getYouTubeDataApi() {
        if(dataApi == null) {
            dataApi = new DataApi();
        }
        return dataApi;
    }

    public static void setUserId(String userId) {
        MyApplication.userId = userId;
    }

    public static String getUserId() {
        return userId;
    }

    public static void setUserName(String userName) {
        MyApplication.userName = userName;
    }

    public static String getUserName() {
        return userName;
    }

    public static YouTubeController getYouTubeController() {
        return youTubeController;
    }

    public static void setYouTubeController(YouTubeController youTubeController) {
        MyApplication.youTubeController = youTubeController;
    }

    public static YouTubeFragment getYouTubeFragment() {
        return youTubeFragment;
    }

    public static void setYouTubeFragment(YouTubeFragment youTubeFragment) {
        MyApplication.youTubeFragment = youTubeFragment;
    }
}
